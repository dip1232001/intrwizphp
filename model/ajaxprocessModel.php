<?php

include_once 'config.php';

class ajaxprocessModel {
    /*
     * reference output data
     * 
      $response['details'] = [
      'AgentLoginName' => 'arkaj5@mailinator.com',
      'AgentLoginPassword' => '12345678',
      'APILoginName' => 'api_arkaj5@mailinator.com',
      'APILoginPassword' => '12345678',
      'CompanyAdminLoginName' => 'admin_arkaj5@mailinator.com',
      'CompanyAdminLoginPassword' => '12345678',
      'CompanyWebSite' => 'test1.dev.takemethere.ca',
      'EmailAddress' => 'arkaj5@mailinator.com',
      'CompanyID' => 542,
      'BtoBWebSite' => 'dev.intrwiz.com',
      ];
     */

    public function register($function_url, $call_type, $request_params, $url_params, $contentType, $debug) {
        $request_params['ControlPanelDomain'] = $request_params['BtoBDomain'] = $request_params['BtoCDomain'] = WEBSITEDOMAIN;
        $response = $this->__curlsetup($function_url, $call_type, $request_params, $url_params, $contentType, $debug);
        if (empty($response['msg'])) {

            $message_type = 26; // 71aa84fd-d640-4bb5-9131-39c286d14329
            $this->regEmail($response, $request_params, $message_type);
        }
        return $response;
        exit;
    }

    public function email($substitution_tags, $emailAddress, $message_type, $fromName = "Intrwiz", $company_id = DEFAULT_COMPANY_ID) {
        $tracking_arguments = null;
        //define("DEFAULT_COMPANY_ID", 527);
        $language = 'en';
        $function_url = API_END_POINT . '/Notification/api/v1/' . 'companies/' . $company_id . '/emailtemplate/' . $message_type . '/language/' . $language . '/sendemailext';

        $call_type = 'POST';
        $request = array(
            'Recipients' => $emailAddress,
            'Arguments' => $tracking_arguments,
            'FromName' => $fromName
        );
        $request['SubstitutionTags'] = $substitution_tags;


        return $result = $this->__curlsetup($function_url, $call_type, $request, [], 'application/json; charset=utf-8', false);
    }

    public function regEmail($response, $request_params, $message_type) {
        global $contactnumbers;
        // print_r($response);
        // $email_api = new EmailApi();
        // $language = new LanguageSelect();
        $substitution_tags['<%subject%>'] = 'Intrwiz Registration Details';
        $substitution_tags['<%user_first_name%>'] = $request_params['FirstName'];
        $substitution_tags['<%COMPANY NAME%>'] = trim($request_params['CompanyName']);
        // $user->details->AgentLoginName;
        $substitution_tags['<%user_last_name%>'] = $request_params['LastName'];
        $substitution_tags['<%b2c_link%>'] = 'http://' . $response['details']['CompanyWebSite'];
        $substitution_tags['<%b2b_link%>'] = rtrim($substitution_tags['<%b2c_link%>'], '/') . '/dashboard'; //'https://newcallcenter.takemethere.ca/login';//'http://' . $response['details']['BtoBWebSite'] . '/login';
        $substitution_tags['<%comp_admin%>'] = $response['details']['CompanyAdminLoginName'];
        $substitution_tags['<%comp_pwd%>'] = $response['details']['CompanyAdminLoginPassword'];
        //$substitution_tags['<%Agency_admin%>'] = $user->details->AgencyAdminName;
        //$substitution_tags['<%Agency_pwd%>'] = $user->details->AgencyLoginPassword;
        $substitution_tags['<%agent%>'] = $response['details']['AgentLoginName'];
        $substitution_tags['<%cp_link%>'] = rtrim($substitution_tags['<%b2c_link%>'], '/') . '/cp/controlpanel';
        $substitution_tags['<%agent_pwd%>'] = $response['details']['AgentLoginPassword'];
        $contactht = '';
        $contactht .= '<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tbody>
											<tr>';
        $x = 0;
        foreach ($contactnumbers as $key => $value) {
            if ($x > 0 && $x % 2 == 0) {
                $contactht .= '</tr><tr>';
            }

            $contactht .= '<td style="font-weight: 700;color: #000000;padding: 6px 0;margin: 0;"><span style="color: rgb(94, 94, 94); font-size: 15px; font-weight: 700;">' . $key . '</span></td>
	<td style="padding: 6px 0;margin: 0;"><a href="#" style="font-size: 15px; color: rgb(94, 94, 94);">' . $value['shownumber'] . '</a></td>';


            $x++;
        }
        $contactht .= '</tr><tr>
												<td style="font-weight: 700;color: #000000;padding: 6px 0;margin: 0;">&nbsp;</td>
												<td style="padding: 6px 0;margin: 0;">&nbsp;</td>
												<td style="font-weight: 700;color: #000000;padding: 6px 0;margin: 0;">&nbsp;</td>
												<td style="padding: 6px 0;margin: 0;">&nbsp;</td>
											</tr>
										</tbody>
									</table>';
        $substitution_tags['<%html_company_numbers%>'] = $contactht;
        $tracking_arguments = null;
        $company_id = $response['details']['CompanyID'];
        $language = 'en';
        // print_r($substitution_tags);
        $function_url = API_END_POINT . '/Notification/api/v1/' . 'companies/' . $company_id . '/emailtemplate/' . $message_type . '/language/' . $language . '/sendemailext';
        $call_type = 'POST';
        $request = array(
            'Recipients' => $response['details']['EmailAddress'],
            //'dts.arkab@dreamztech.com',//'dipankardts@dreamztech.com',//
            'Arguments' => $tracking_arguments,
        );
        if ($message_type == 26) {
            $request['FromName'] = 'Intrwiz';
        }


        $request['SubstitutionTags'] = $substitution_tags;

        //print_r($substitution_tags);
        //exit;

        $result = $this->__curlsetup($function_url, $call_type, $request, [], 'application/json; charset=utf-8', false);
    }

    private function __curlsetup($function_url, $call_type, $request_params, $url_params = [], $contentType, $debug) {
        $response = "";
        $url_params = array_merge(array('apiaccount' => '5774e12c-2bef-43f7-a4e2-15d28a11c741'), $url_params);

        $url_query = http_build_query($url_params);

        $call_url = $function_url . "?" . $url_query;


        $body_str = json_encode($request_params);

        $headers = array(
            'Content-type: ' . $contentType
        );



        //Set the CURL Options
        $curl_opts = array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_URL => $call_url,
            CURLOPT_CUSTOMREQUEST => $call_type,
            CURLOPT_CONNECTTIMEOUT => 3,
            CURLOPT_TIMEOUT => 500,
        );

        //If the type is PUT or POST, we need to add the body str
        // if (self::CALL_TYPE_PUT == $call_type || self::CALL_TYPE_POST == $call_type || self::CALL_TYPE_DELETE == $call_type) {
        $curl_opts[CURLOPT_POSTFIELDS] = $body_str;
        //  }
        //Make the call
        //Set up the CURL request
        $apiCurl = curl_init();
        curl_setopt_array($apiCurl, $curl_opts);

        // Send the request & save response to $resp
        $res = curl_exec($apiCurl);
        $info = curl_getinfo($apiCurl);
        $err_no = curl_errno($apiCurl);
        $httpcode = curl_getinfo($apiCurl, CURLINFO_HTTP_CODE);
        curl_close($apiCurl);

        if ($debug) {
            var_dump($curl_opts, $res, $info, $err_no);
            print $body_str;
            die();
        }

        //If curl returned an error, abort, otherwise, save the token
        if (0 != $err_no) {
            App::abort(500, 'Error In API Call "' . $function_url . '" - Curl Err:' . $err_no);
        } else {
            $response = json_decode($res, true);
            //$response = ($return_raw) ? $res : json_decode($res, true);
            if ($httpcode !== 200) {
                $response = (is_null($response) || empty($response)) ? $httpcode : $response;
            }
        }
        return $response;
    }
    
    public function getAllCountries() {
        $api_function = API_END_POINT."/Bus/api/v1/getcountryphonecode";
        return $this->__curlsetup($api_function, 'GET');
    }
    
    private $mainCurrencies = [
        0 => 'USD',
        1 => 'EUR',
        2 => 'GBP',
        3 => 'CAD',
        4 => 'AUD',
        5 => 'SEK'
    ];
    
    public function getCurrencies() {
        //$this->uses_admin_api = true;
       
        $api_function = API_END_POINT."/Admin/api/v1/configs/currencycodes";
        $types = $this->__curlsetup($api_function, 'GET', array(), array());
        $ret_array = $types;
        if (is_array($ret_array)) {
            usort($ret_array, function($a, $b) {
                return strcmp($a->CurrencyName, $b->CurrencyName);
            });
        } else {
            return [];
        }
        return $ret_array;
    }

}

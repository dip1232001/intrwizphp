<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Intrwiz-Marketing</title>

    <!-- Bootstrap Core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/custom.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <!-- Navigation Start -->    
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="top-nav">
            <div class="container">
                <ul>
                    <li><a href="#">Sign In</a></li>  
                    <li><a href="#" class="reg-btn">Register</a></li>                  
                </ul>                
            </div>
        </div>
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html"><img src="img/logo.png" alt="" class="img-responsive"></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="using-intrwiz.html">Using Intrwiz</a>
                    </li>
                    <li>
                        <a href="pricing.html">Pricing</a>
                    </li>
                    <li>
                        <a href="global-partners.html">Global Partners</a>
                    </li>
                    <li>
                        <a href="press.html">News</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <!-- Navigation End -->

    <!-- Body Section Start -->
    <div id="inner-body-wrap">
        <div class="container">
            <div class="contact-wrap">
                <div class="row">
                    <div class="col-sm-12 col-md-8 col-lg-8">
                        <h2>Contact Us</h2>
                        <p>We would love to here from you. Please use the form below to send us a comment or ask a question. We will reply as soon as possible.</p>
                        <p>You can also give us a call at:</p>
                        <div class="contact-list">
                            <div class="row">
                                <div class="col-sm-12 col-md-2 col-lg-2"><span>USA</span></div>
                                <div class="col-sm-12 col-md-4 col-lg-4"><a href="#">+1 -(800)- 799-4451</a></div>
                                <div class="col-sm-12 col-md-2 col-lg-2"><span>Pakistan</span></div>
                                <div class="col-sm-12 col-md-4 col-lg-4"><a href="#">1 -123-123-1234</a></div>
                                <div class="col-sm-12 col-md-2 col-lg-2"><span>Canada</span></div>
                                <div class="col-sm-12 col-md-4 col-lg-4"><a href="#">+1 -(647)- 799-0250</a></div>
                                <div class="col-sm-12 col-md-2 col-lg-2"><span>Kenya</span></div>
                                <div class="col-sm-12 col-md-4 col-lg-4"><a href="#">1 -123-123-1234</a></div>
                                <div class="col-sm-12 col-md-2 col-lg-2"><span>Mexico</span></div>
                                <div class="col-sm-12 col-md-4 col-lg-4"><a href="#">+52 -(55)- 85261991</a></div>
                                <div class="col-sm-12 col-md-2 col-lg-2"><span>Tanzani</span></div>
                                <div class="col-sm-12 col-md-4 col-lg-4"><a href="#">1 -123-123-1234</a></div>
                                <div class="col-sm-12 col-md-2 col-lg-2"><span>London</span></div>
                                <div class="col-sm-12 col-md-4 col-lg-4"><a href="#">+44 -(20)- 83966666</a></div>
                                <div class="col-sm-12 col-md-2 col-lg-2"><span>Uganda</span></div>
                                <div class="col-sm-12 col-md-4 col-lg-4"><a href="#">1 -123-123-1234</a></div>
                                <div class="col-sm-12 col-md-2 col-lg-2"><span>Kuwait</span></div>
                                <div class="col-sm-12 col-md-4 col-lg-4"><a href="#">1 -123- 123-1234</a></div>
                                <div class="col-sm-12 col-md-2 col-lg-2"><span>Australia</span></div>
                                <div class="col-sm-12 col-md-4 col-lg-4"><a href="#">1 -123-123-1234</a></div>
                                <div class="col-sm-12 col-md-2 col-lg-2"><span>Sweden</span></div>
                                <div class="col-sm-12 col-md-4 col-lg-4"><a href="#">1 -123-123-1234</a></div>
                                <div class="col-sm-12 col-md-2 col-lg-2"><span>New Zealand</span></div>
                                <div class="col-sm-12 col-md-4 col-lg-4"><a href="#">1 -123-123-1234</a></div>
                                <div class="col-sm-12 col-md-2 col-lg-2"><span>UAE</span></div>
                                <div class="col-sm-12 col-md-4 col-lg-4"><a href="#">1 -123-123-1234</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-4">
                        <div class="gray-box">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                    <label>First Name</label>
                                    <input type="text" name="firstname" id="">
                                </div>
                                <div class="col-sm-12 col-md-6 col-lg-6">
                                <label>Last Name</label>
                                    <input type="text" name="firstname" id="">
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                <label>Email Address</label>
                                    <input type="text" name="firstname" id="">
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                <label>Message</label>
                                    <textarea name="message" id=""></textarea>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                <a href="#" class="pink-button">Send Message</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Body Section End -->

    <!-- Footer Section Start -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <h3>Follow Us</h3>
                    <ul class="social-icons">
                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12">
                    <h3>Quick Links</h3>
                    <ul>
                        <li><a href="agent-platform.html">Agent Platform</a></li>
                        <li><a href="#">Closed Group Affinity Site</a></li>
                        <li><a href="full-end-to-end-custom.html">End to End custom Booking Site</a></li>
                        <li><a href="pricing.html">Pricing</a></li>
                        <li><a href="#">Commissions</a></li>
                        <li><a href="using-intrwiz.html">Using Intrwiz</a></li>
                        <li><a href="#">Resources</a></li>
                        <li><a href="press.html">News</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12">
                    <h3>Contact Us</h3>
                    <ul>
                        <li><a href="#">Contact Us</a></li>
                        <li><a href="#">Carrer Inquiries</a></li>
                        <li><a href="#">Affiliate Inquiries</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- Script to Activate the Carousel -->

</body>
</html>

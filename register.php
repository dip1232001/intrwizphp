<?php 
include_once 'model/ajaxprocessModel.php';
$model = new ajaxprocessModel();
$countryList = $model->getAllCountries();
$currencyList = $model->getCurrencies();

?>
<!DOCTYPE html>
<html>
    <head>

        <title>Intrwiz</title>
        <!-- Meta -->
        <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Bootstrap -->

        <!--        <link type="text/css" rel="stylesheet" href="https://app.intrwiz.com/min/?g=mainbootstrap" />
                 Bootstrap 
                
        
                 Custom 
                <link type="text/css" rel="stylesheet" href="https://app.intrwiz.com/min/?g=headmaincustomstyles" />-->

        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/custom.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">





        <script src="js/jquery.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
        <style>
            .error {
                border: 2px solid #f00;
            }

            .valid {
                border: 2px solid #0ff;
            }
            input:focus,textarea:focus{
                border-color: initial;
            }

            input.error, textarea.error{
                border:1px solid red;
            }
        </style>

        <!-- Script -->

        <!-- device selector -->
        <script>
            $('document').ready(function () {
                submitfreeRegisterFormFunc = function ($event) {
                    var element = $($event.currentTarget);

                    //  alert('aa');
                    //  $('#free_registr_form').find('.error').val(' ');
                    $('input, textarea').click(function () {
                        //  $(this).removeClass('error').val('');
                        $(this).removeClass('error');
                    });
                    $('input, textarea').blur(function () {
                        //  $(this).removeClass('error').val('');
                        $(this).removeClass('error');
                    });
                    $('input, textarea').focus(function () {
                        //  $(this).removeClass('error').val('');
                        $(this).removeClass('error');
                    });

                    var fields = ["CompanyName", "FirstName", "LastName", "email", "ContactNumber", "password", "ConfirmPassword"]// 

                    var i, l = fields.length;
                    var fieldname;
                    var noError = true;
                    var password = $("#password").val();
                    var ConfirmPassword = $("#ConfirmPassword").val();
                    for (i = 0; i < l; i++) {
                        fieldname = fields[i];
                        if (document.forms["free_registr_form"][fieldname].value === "") {

                            $('#' + fieldname).addClass('error');

                            //  alert(fieldname + " can not be empty");
                            noError = false;
                        }
                        if (fieldname === "email" && document.forms["free_registr_form"][fieldname].value != "") {
                            var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                            if (document.forms["free_registr_form"][fieldname].value.match(mailformat))
                            {
                                //  alert('ok');

                            } else {
                                noError = false;
                                $('#' + fieldname).addClass('error');
                                //  alert('notok');
                            }

                        }
                        if (password.length < 6) {
                            $('#password').addClass('error');
                            noError = false;
                        } else {
                            if (password != ConfirmPassword) {
                                $('#ConfirmPassword').addClass('error');
                                noError = false;
                            }
                        }
                    }
                    if (noError) {
                        element.button('loading');
                        $.ajax({
                            type: "POST",
                            url: "/ajaxprocess.php",
                            data: $("#free_registr_form").serialize(),
                            dataType: "JSON",
                            success: function (data) {
                                element.button('reset');
                                console.log('success');
                                console.log(data);
                                // $("#yangu_for_free_new").scrollTop(0);
                                $('#yangu_for_free_new').animate({scrollTop: 0}, 'slow');
                               

                                $("#submitsuccess").hide();
                                $("#submiterror").hide();
                                if (data.hasOwnProperty('Message') && data.Message !== '') {
                                    $("#submiterror").html(data.Message);
                                    $("#submiterror").show();
                                } else {
                                    if (data.msg == '') {
                                        $("#submitsuccess").html("Registration is done successfully. Please check your email.");
                                        $("#submitsuccess").show();
                                        window.setTimeout(function () {
                                    window.location.href = '/index.php';
                                }, 5000);
                                    } else {
                                        $("#submiterror").html(data.msg);
                                        $("#submiterror").show();
                                    }

                                }
                                // 
                            },
                            error: function (data) {
                                element.button('reset');
                                console.log('error');
                                console.log(data);
                            }
                        });
                    }





//                    $('#free_registr_form').validate({// initialize the plugin
//                        rules: {
//                            CompanyName: "required",
//                            FirstName: "required",
//                            LastName: "required",
////                            email: {
////                                required: true,
////                                email: true
////                            },
////                            ContactNumber: "required",
////                            password: {
////                                required: true,
////                                minlength: 6
////                            },
////                            ConfirmPassword: {
////                                required: true,
////                                minlength: 6,
////                                equalTo: "#password"
////                            }
//
//                        },
//                        errorPlacement: function (error, element) {
//                            error.insertAfter(element);
//
//                        },
//                        submitHandler: function (form) { // for demo
//                            alert('valid form submitted'); // for demo
//                            return false; // for demo
//                            $.ajax({
//                                type: "POST",
//                                url: "/ajaxprocess.php",
//                                data: $("#free_registr_form").serialize(),
//                                success: function () {
//                                    //console.log('success');
//                                },
//                                complete: function () {
//                                    //console.log('complete');
//                                    //location.reload();
//                                },
//                                error: function (data) {
//                                    //console.log(data);
//                                }
//                            });
//                        }
//                    });
                }


//                $.ajax({
//                    type: "POST",
//                    url: "/mobile-select",
//                    data: {
//                        "mobile_data": JSON.stringify(mobile_data)
//                    },
//                    success: function () {
//                        //console.log('success');
//                    },
//                    complete: function () {
//                        //console.log('complete');
//                        //location.reload();
//                    },
//                    error: function (data) {
//                        //console.log(data);
//                    }
//                });
            });
        </script>        <script>
            $(document).ready(function () {

                $('body').addClass('modal-open');
                $('#yangu_for_free_new').css({display: 'block'});
            });
        </script>





    </head>
    <!-- begin body output -->
    <body>
        <!-- Google Tag Manager (noscript) -->

        <!-- End Google Tag Manager (noscript) -->
        <style>
            html,body{
                background: #41838B;
            }
            footer{
                display: none;
            }
        </style>
        <div >
            <!-- Login Modal -->
            <div id="yangu_for_free_new"  class="modal fade login in login-modal" tabindex="-1" role="dialog" aria-hidden="false">
                <div class="modal-dialog">
                    <div class="modal-content login_modal">
                        <div class="modal-header">
                            <button id="closing-modal" type="button" class="close btn-modal-close" onclick="location.href = '/';"><span class="modal-text black close-text" >Close </span><span class="pink">&times;</span></button>
                            <!-- <p><img src="images/logo.png" alt="Logo"/></p> -->
                            <h2 class="modal-title black left-align">Register</h2>
                            <p class="descr left-align">
                                (All fields are required) <span class="pink">*</span>
                            </p>
                        </div><!-- /.modal-header -->
                        <form method="POST" id='free_registr_form' name="free_registr_form">
                            <input type="hidden" name="action" value="register">
                            <div class="modal-body black">
                                <div id="submitsuccess" class="alert alert-success" style="display:none;">

                                </div>
                                <div id="submiterror" class="alert alert-danger" style="display:none;">

                                </div>

 <!--<input type="hidden" id="token" name="_token" value="BwGkusxWr2MJsbli35fYiLm0OqRa85uKByOva780">-->   

                                <div class="free-register-box">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="field_row">
                                                <div>
                                                    <h5 class="display-in">Company Name </h5>
                                                    <span class="asteric-red">*</span>
                                                </div>
                                                <input type="text" id="CompanyName" required class="form-control left-align" name="CompanyName" value=""  />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="field_row">
                                                <div>
                                                    <h5 class="display-in">First Name</h5>
                                                    <span class="asteric-red">*</span>
                                                </div>
                                                <input type="text" id="FirstName" required class="form-control left-align" name="FirstName" value=""  />
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="field_row">
                                                <div>
                                                    <h5 class="display-in">Last Name</h5>
                                                    <span class="asteric-red">*</span>
                                                </div>
                                                <input id="LastName" type="text" required class="form-control left-align" name="LastName"  />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="field_row">
                                                <div>
                                                    <h5 class="display-in">Email Address</h5>
                                                    <span class="asteric-red">*</span>
                                                </div>
                                                <input type="email" id="email" required class="form-control left-align" name="email" value=""  />
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="field_row">
                                                <div>
                                                    <h5 class="display-in">Contact Number</h5>
                                                    <span class="asteric-red">*</span>
                                                </div>
                                                <input type="text" id="ContactNumber" autocomplete="off"  required class="form-control left-align" name="ContactNumber" value=""  />
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row hidden">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="field_row">
                                                <div>
                                                    <h5 class="display-in">Country</h5>
                                                    
                                                </div>
<!--                                                -->
                
                                                
                                                <select class="form-control" id="CompanyCountry" name="CompanyCountry">
                                                    <option value="">Select Country</option>
                                                    <?php 
                                                    foreach ($countryList as $k=>$v){
                                                        ?>
                                                    <option value="<?php echo $v['Code'];?>"><?php echo $v['Name'];?> (<?php echo $v['Code'];?>)</option>
                                                        <?php
                                                    }
                                                    ?>
                                                    
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="field_row">
                                                <div>
                                                    <h5 class="display-in">Currency</h5>
                                                    
                                                </div>
                                                <select class="form-control" id="CompanyCurrency" name="CompanyCurrency">
                                                    <option  value="">Select Currency</option>
                                                    <?php 
                                                    foreach ($currencyList as $k=>$v){
                                                        ?>
                                                    <option value="<?php echo $v['CurrencyCode'];?>"><?php echo $v['CurrencyName'];?> (<?php echo $v['CurrencyCode'];?>)</option>
                                                        <?php
                                                    }
                                                    ?>
                                                    
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                                <div class="free-register-box">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="field_row">
                                                <div>
                                                    <h5 class="display-in">Password</h5>
                                                    <span class="asteric-red">*</span>
                                                </div>
                                                <input type="password" id="password" minlength="8" autocomplete="off" required class="form-control left-align" name="password" value=""  />
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="charactertxt">
                                                <div class="bot-txt">Minimum 8 Character required</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="field_row">
                                                <div>
                                                    <h5 class="display-in">Confirm Password</h5>
                                                    <span class="asteric-red">*</span>
                                                </div>
                                                <input type="password" id="ConfirmPassword" required class="form-control left-align" name="ConfirmPassword" value=""  />
                                            </div>
                                        </div>

                                    </div>
                                </div>


                                <!-- /.email-login -->
                            </div><!-- /.modal-body -->
                            <div class="modal-footer">

                                <div class="paymentMethodRequest">

                                    <h3>Preferred Form of Payment</h3>
                                    <div class="caption">
                                        By default, sites are created to be used for credit card purchases
                                    </div>

                                    <div class="modeOfPayemntGroup">


                                        <ul class="radio_list">
                                            <li>
                                                <label class="control control-checkbox">Credit Card                                
                                                    <input checked="true" disabled="true" type="checkbox" name="preferredPaymentMode[]" value="credit_card" class="">
                                                    <div class="control_indicator"></div>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="control control-checkbox">Apply for Line of Credit                                
                                                    <input type="radio" name="preferredPaymentMode[]" value="line_of_credit" class="">
                                                    <div class="control_indicator"></div>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="control control-checkbox">Floating Deposit                                
                                                    <input type="radio" name="preferredPaymentMode[]" value="floating_deposit" class="">
                                                    <div class="control_indicator"></div>
                                                </label>
                                            </li>
                                        </ul>


                                    </div>
                                </div>

                                <!--<button type="submit" class="btn btn-lg btn-pink pull-left">Sign-In</button>-->
                                <button type="button" id="registerbtn" onclick="submitfreeRegisterFormFunc(event)"  class="btn btn-lg btn-pink pull-right" data-loading-text="Register <i class='fa fa-circle-o-notch fa-spin'></i> " />Register</button>
                                <!--<button type="submit" id="submit_user_login" onclick="agentLogin()" class="btn btn-lg btn-pink pull-left">Sign-In</button>-->
                                <!-- <div class="not-registered descr pull-left">Not registered? <a class="pink underlined switch-to-register" onclick="switchToregister()">Register now</a></div> -->
                            </div><!-- /.modal-footer -->
                        </form>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>


        </div>



        <!-- end body output -->

    </body>
</html>
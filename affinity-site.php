 <?php include_once 'partials/head.php'; ?>
<?php include_once 'partials/navbar.php'; ?>

<!-- Body Section Start -->
<div id="inner-body-wrap">
    <div class="container">
        <div class="collaborate-solution">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="left-content-panel">
                        <h2>Closed User Group Affinity Site</h2>
                        <ul>
                            <li>Branded Affinity Site at net rates.</li> 
                            <li>Control panel to fully Customize platform including setting markups to the net rates.</li>
                            <li>Dashboard to manage bookings.</li> 
                            <li>24/7 call centre support (optional)</li>
                        </ul>
                        <?php /* ?>
                        <div class="pricing-text-section">
                            <p>Pricing</p>
                            <h3>$5000 <sub>setup fee per site</sub></h3>
                            <h4>Contact us to set up your Closed Group Affinity Site.</h4>
                        </div>
                        <?php */ ?>
                        <a href="javascript:void(0)" onclick="$('#modal__contact').modal('show');" class="red-button">Contact Us</a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="sample-clients-panel">
                        <h3 class="text-center">Sample Clients</h3>
                        <!-- sample row -->
                        <div class="row mb20">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <img src="img/thumbnail-atcost.jpg" class="thumbnail-img img-responsive">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="sample-logo-panel">
                                    <img src="img/at-cost-logo.jpg" class="img-responsive">
                                    <p><a href="<?php echo ATCOST_LINK?>" target="_blank">View Live Site</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="row mb20">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <img src="img/jubili.png" class="thumbnail-img img-responsive">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="sample-logo-panel">
                                    <img src="img/JHA-logo-color.svg" class="img-responsive">
                                    <p><a href="<?php echo JUBILEE_LINK?>" target="_blank">View Live Site</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="row mb20">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <img src="img/thumbnail-airmiles.jpg" class="thumbnail-img img-responsive">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="sample-logo-panel">
                                    <img src="img/air-miles-logo.jpg" class="img-responsive">
                                    <p><a href="<?php echo AIRMILES_LINK?>" target="_blank">View Live Site</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body Section End -->

<?php include_once 'partials/footer.php'; ?>

</body>
</html>

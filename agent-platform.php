<?php include_once 'partials/head.php'; ?>
<?php include_once 'partials/navbar.php'; ?>

<div id="inner-body-wrap">
    <!-- Agent Platform Section Start -->
    <div class="section-agent-dashboard">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <div class="wh-box-contain">
                    <div class="left-content-panel">
                        <h2>Agent Platform</h2>
                        <ul>
                            <li>Branded State of the Art Collaborative B2B and B2C Booking Solutions.</li>
                            <li>Fully Integrated Interactive Agent Dashboard.</li>
                            <li>100’s of World Class Content Providers to choose from.</li>
                        </ul>
                        <?php /* ?><a <?php echo (IS_PROD) ? "onclick='$(\"#demo_site_msg\").modal(\"show\")' href='javascript:void(0);'" : 'href="/register.php"'; ?> class="red-button">Register For Free</a><?php */ ?>
                        <a onclick="$('#modal__contact').modal('show')" href="javascript:void(0);" class="red-button">Contact Us</a>
                       <!-- <a target="_blank" href="/manual/index.html" class="red-button">Get Started</a>-->
                    </div>
                </div>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-12 no-pad"><img src="img/laptop-allthree.png" class="img-responsive"></div>
        </div>
    </div>
    <!-- Agent Platform Section End -->

    <!-- Core Pieces Section Start -->
    <div class="section-core-pieces">
        <div class="container">
            <h2 class="text-center">The 3 core pieces of your new system:</h2>
            <ul>
                <li><img src="img/home/icon-paper-plane.png"> Online Booking Platform</li>
                <li><img src="img/home/icon-dashboard.png"> Dashboard</li>
                <li><img src="img/home/icon-controlpanel.png"> Control Panel</li>
            </ul>
        </div>
    </div>
    <!-- Core Pieces Section End -->

    <!-- Client Portal Section Start -->
    <div class="section-client-portal">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 no-pad">
                <img src="img/home/laptop-clientfacingportal.png" class="img-responsive">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="gray-box-contain">
                    <h3><img src="img/home/icon-paper-plane.png">Your Online Booking Platform  </h3>
                    <p>State of the Art Collaborative Interactive Booking Solutions for Air, Hotel, Car, Activities,
Tours and Cruise directly contented to hundreds of suppliers in the Intrwiz Eco System.</p>

<p>Each search automatically creates a Branded Interactive Micro Site directly connected
to your Agent Dashboard allowing you to seamlessly engage in real time with each
customer through the planning and booking process.</p>

                    <?php if (DEMO_LINK_TO_SHOW) { ?>
                        <?php if (IS_PROD) { ?>
                            <!--onclick='$("#demo_site_msg").modal("show")'-->
                            <!-- <a  href='pricing.php'  class="btn-details btn-details-sm">Order</a>-->
                        <?php } else { ?>
                            <a href="<?php echo TEST_DEMO_SITE; ?>" target="_blank" class="btn-details btn-details-sm">View Demo</a>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Client Portal Section End -->

    <!-- Dashboard Section Start -->
    <div class="section-dashboard" >
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 text-right">
                <div class="wh-box-contain">
                    <h3><img src="img/home/icon-dashboard.png">Your Interactive Agent Dashboard  </h3>
                    <p>Engage and manage your customers seamlessly on your Intrwiz Interactive Agent
Dashboard where you can interact with each of your customers in real time providing a
unique end to end experience that will keep your customers coming back to you forever.
<p>
Each Branded Interactive Customer Micro Site is connected to your agent dashboard
providing you with unparalleled business intelligence to convert all your opportunities
and maximize sales on each trip.</p>
<p>
Branded Micro Sites can be created via each booking on your GDS, every search on
the Intrwiz Online Booking Solutions or can be created from the Agent Dashboard.</p>




                    <?php if (DEMO_LINK_TO_SHOW) { ?>
                        <?php if (IS_PROD) { ?>

                            <!-- <a  href='pricing.php'  class="btn-details btn-details-sm">Order</a>-->
                        <?php } else { ?>
                            <p>
                                <a href="<?php echo TEST_DEMO_SITE; ?>dashboard/login" target="_blank" class="btn-details btn-details-sm pull-right">View Demo</a>
                            <div class="clearfix"></div>
                            </p>
                            <p class="text-right"><b>Login:</b> <?php echo AGENT_DASHBOARD_LOGIN; ?> | <b>Password:</b> <?php echo AGENT_DASHBOARD_PASS; ?></p>

                        <?php } ?>
                    <?php } ?>



                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 no-pad"><img src="img/home/laptop-dashboard.png" class="img-responsive"></div>
        </div>
    </div>
    <!-- Dashboard Section End -->

    <!-- Control Panel Section Start -->
    <div class="section-control-panel" >
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 no-pad">
                <img src="img/home/laptop-controlpanel.jpg" class="img-responsive">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="gray-box-contain">
                    <h3><img src="img/home/icon-controlpanel.png"> Your Control Panel  </h3>
                    <p>Customize your client facing portal and each Branded Interactive Customer Micro Site including look and feel, messaging, suppliers, booking engines, pricing, email notifications and many other intuitive features to help you create a unique branded experience for your customers.   

                    </p>

                    <?php if (DEMO_LINK_TO_SHOW) { ?>
                        <?php if (IS_PROD) { ?>

                            <!-- <a  href='pricing.php'  class="btn-details btn-details-sm">Order</a>-->
                        <?php } else { ?>
                            <p>
                                <a href="<?php echo TEST_DEMO_SITE; ?>cp/controlpanel"  target="_blank" class="btn-details btn-details-sm">View Demo</a>
                            </p>
                            <p><b>Login:</b> <?php echo CP_LOGIN; ?> | <b>Password:</b> <?php echo CP_PASS; ?></p>
                        <?php } ?>
                    <?php } ?>





                </div>
            </div>
        </div>
    </div>
    <!-- Control Panel Section End -->

    <!-- Commisions Section Start -->
    <div class="section-Commisions text-center hidden" id="agent-commisions">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <h2 class="text-center">Intrwiz Commissions</h2>
                    <p>We Manage the full booking process including 24/7 customer service.   You simply receive your commission check bi-weekly on all confirmed sales (commissions are calculated on the gross selling price before taxes) </p>
                    <div class="commisions-list-wrap">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="sub-wh-box">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2 col-sm-12"><img src="img/home/svg/icon-flight.svg"></div>
                                        <div class="col-lg-5 col-md-5 col-sm-12 no-pad">
                                            <h2>Flights</h2>
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-sm-12">
                                            <div class="pink-percent-txt" style="text-transform: none;">Up to 25%</div>
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-sm-12">
                                            <div class="pink-percent-txt hide">$5.00<span>per private air booking</span></div>
                                            <div class="pink-percent-txt hide">TBD</div>
                                        </div>
                                    </div>
                                    <ul>
                                        <li>Direct Negotiated fares with over 750 airlines </li>
                                        <!--                                        <li>70+ point of sale countries</li>
                                                                                <li>190+ Countries Connected</li>-->
                                        <li>Book on your GDS and Que your bookings to your agent dashboard</li>
                                        <li class="hide">Add your additional markups of up to 50% on Intrwiz private negotiated fares and still be competitive</li>
                                        <li>Access our direct net rates and add your markups to earn as much as 25% commissions and still be competitive. If you have a floating deposit in place with us, your pricing will reduce by an additional 3% for credit card fees saved.</li>
                                        <li>24/7 Call Centre Support  </li>

                                    </ul>
                                </div>
                                <div class="sub-wh-box">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2 col-sm-12"><img src="img/home/svg/icon-car.svg"></div>
                                        <div class="col-lg-5 col-md-5 col-sm-12 no-pad">
                                            <h2>Cars</h2>
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-sm-12">
                                            <div class="pink-percent-txt">8%</div>
                                        </div>
                                    </div>
                                    <ul>

                                        <li>Direct negotiated rates with all major car suppliers  </li>
                                        <li>Coverage to over 250 countries </li>
                                        <li>24/7 Call Centre Support  </li>
                                    </ul>
                                </div>
                                <div class="sub-wh-box">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2 col-sm-12"><img src="img/home/svg/icon-cruise.svg"></div>
                                        <div class="col-lg-5 col-md-5 col-sm-12 no-pad">
                                            <h2>Cruises</h2>
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-sm-12">
                                            <div class="pink-percent-txt">12%</div>
                                        </div>
                                    </div>
                                    <ul>

                                        <li>Directly connected to all major cruise lines.</li>
                                        <li>Global Fulfilment  </li>
                                        <li>24/7 Call Centre Support  </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="sub-wh-box">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2 col-sm-12"><img src="img/home/svg/icon-hotel.svg"></div>
                                        <div class="col-lg-5 col-md-5 col-sm-12 no-pad">
                                            <h2>Hotels</h2>
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-sm-12">
                                            <div class="pink-percent-txt" style="text-transform: none;">Up to 25%</div>
                                        </div>
                                    </div>
                                    <ul>
                                        <li>Direct Negotiated rates for over 1 million hotels</li>
                                        <li>Coverage over 240 countries</li>
                                        <li>Access our direct net rates and add your markups to earn as much as 25% commissions and still be competitive. If you have a floating deposit in place with us, your pricing will reduce by an additional 3% for credit card fees saved.</li>
                                        <li>24/7 Call Centre Support  </li>
                                    </ul>
                                </div>
                                <div class="sub-wh-box">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2 col-sm-12"><img src="img/home/svg/icon-camera.png"></div>
                                        <div class="col-lg-5 col-md-5 col-sm-12 no-pad">
                                            <h2>Activities</h2>
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-sm-12">
                                            <div class="pink-percent-txt">8%</div>
                                        </div>
                                    </div>
                                    <ul>
                                        <li>Handpicked world class activities from around the world</li>
                                        <li>Coverage to over 300 Countries.  </li>
                                        <li>24/7 Call Centre Support  </li>
                                    </ul>
                                </div>
                                <div class="sub-wh-box">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2 col-sm-12"><img src="img/home/svg/icon-tours.svg"></div>
                                        <div class="col-lg-5 col-md-5 col-sm-12 no-pad">
                                            <h2>Tours</h2>
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-sm-12">
                                            <div class="pink-percent-txt">12%</div>
                                        </div>
                                    </div>
                                    <ul>
                                        <li>Multi Day tours for every type of experience powered by world class Tour Operators from around the world</li>
                                        <li>Coverage to over 200 countries</li>
                                        <li>24/7 Call Centre Support  </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Commisions Section End -->

   <?php include_once 'partials/intrwizEcosystem.php'; ?>
    
    <!-- Caption Section Start -->
    <?php /* ?>
    <div class="section-caption">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                    <p>Smart, Simple, Social</p>
                    <div class="btn-row text-center top-pad100">
                        <a href="pricing.php" class="btn-details">See Pricing</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php */ ?>
    <!-- Caption Section END -->
</div>

<?php include_once 'partials/footer.php'; ?>

</body>
</html>

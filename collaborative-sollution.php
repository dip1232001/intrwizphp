<?php include_once 'partials/head.php'; ?>
<?php include_once 'partials/navbar.php'; ?>

<!-- Body Section Start -->
<div id="inner-body-wrap">
    <div class="container">
        <div class="collaborate-solution">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="left-content-panel">
                        <h2>Collaborative Itinerary Solution</h2>
                        <ul>
                            <li>Maximize Ancillary Revenue Streams.</li> 
                            <li>Provide a world class experience to your customers.</li>
                            <li>Integrated with the Intrwiz Call Centre Dashboard to seamlessly manage each booking</li> 
                            <li>Loaded with Rich destination content powered by ArrivalGuides.</li>
                            <li>Use Intrwiz commissionable content or your own.</li>
                        </ul>
                        <div class="pricing-text-section">
                            <h4>To set up your enhanced collaborative itineraries, please contact us.</h4>
                        </div>
                        <a href="javascript:void(0)" onclick="$('#modal__contact').modal('show');" class="red-button">Contact Us</a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <h3 class="text-center">Sample Clients</h3>
                    <div class="thubmnail-img">
                        <img src="img/thumbnail-Ag-destination.png" class="img-responsive">
                        <p><a href="https://booking.arrivalguides.com/en/TravelGuides/Europe/United%20Kingdom/LONDON" target="_blank">View Itinerary</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body Section End -->

<?php include_once 'partials/footer.php'; ?>

</body>
</html>

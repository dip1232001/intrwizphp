<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><meta name="viewport" content="width=device-width">
    <title>Intrwiz</title>
</head>
<body style="-moz-box-sizing: border-box; -ms-text-size-adjust: 100%; -webkit-box-sizing: border-box; -webkit-text-size-adjust: 100%; Margin: 0; box-sizing: border-box; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; min-width: 100%; padding: 0; text-align: left; width: 100% !important;">
<style type="text/css">@media only screen {
      html {
        min-height: 100%;
        background: #f3f3f3;
      }
    }
   
    .only-mobile
      {
          display:none !important;
        }
        .only-desktop
      {
          display:table-cell !important;
        }
        .footer a[href^=tel], .footer a[href^=mailto], .ii a[href]{
            color: #fff !important;
        }
       
        .mail-header a[href^=tel], .mail-header a[href^=mailto], .ii a[href]{
            color: #fff !important;
        }
   
    @media only screen and (max-width: 596px) {
      .small-float-center {
        margin: 0 auto !important;
        float: none !important;
        text-align: center !important;
      }
      .small-text-center {
        text-align: center !important;
      }
      .small-text-left {
        text-align: left !important;
      }
      .small-text-right {
        text-align: right !important;
      }
      .only-mobile
      {
          display:table-cell !important;
        }
        .only-desktop
      {
          display:none !important;
        }
        #get-started
        {
           
            padding-left:25px !important;
        }
       
        #get-started a
        {
            width:100% !important;
            text-align: center !important;

        }
       
        .wide
        {
            width: 100% !important;
        }
       
        .footer
        {
            width:100% !important;
            display:block !important;
            border-right:none !important;
        }
    }
   
    @media only screen and (max-width: 596px) {
      .hide-for-large {
        display: block !important;
        width: auto !important;
        overflow: visible !important;
        max-height: none !important;
        font-size: inherit !important;
        line-height: inherit !important;
      }
    }
   
    @media only screen and (max-width: 596px) {
      table.body table.container .hide-for-large,
      table.body table.container .row.hide-for-large {
        display: table !important;
        width: 100% !important;
      }
    }
   
    @media only screen and (max-width: 596px) {
      table.body table.container .callout-inner.hide-for-large {
        display: table-cell !important;
        width: 100% !important;
      }
    }
   
    @media only screen and (max-width: 596px) {
      table.body table.container .show-for-large {
        display: none !important;
        width: 0;
        mso-hide: all;
        overflow: hidden;
      }
    }
   
    @media only screen and (max-width: 596px) {
      table.body img {
        width: auto;
        height: auto;
      }
      table.body center {
        min-width: 0 !important;
      }
      table.body .container {
        width: 95% !important;
      }
      table.body .columns,
      table.body .column {
        height: auto !important;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        padding-left: 16px !important;
        padding-right: 16px !important;
      }
      table.body .columns .column,
      table.body .columns .columns,
      table.body .column .column,
      table.body .column .columns {
        padding-left: 0 !important;
        padding-right: 0 !important;
      }
      table.body .collapse .columns,
      table.body .collapse .column {
        padding-left: 0 !important;
        padding-right: 0 !important;
      }
      td.small-1,
      th.small-1 {
        display: inline-block !important;
        width: 8.33333% !important;
      }
      td.small-2,
      th.small-2 {
        display: inline-block !important;
        width: 16.66667% !important;
      }
      td.small-3,
      th.small-3 {
        display: inline-block;
        width: 25% !important;
      }
      td.small-4,
      th.small-4 {
        display: inline-block ;
        width: 33.33333% ;
      }
      td.small-5,
      th.small-5 {
        display: inline-block !important;
        width: 41.66667% !important;
      }
      td.small-6,
      th.small-6 {
        display: inline-block !important;
        width: 50% !important;
      }
      td.small-7,
      th.small-7 {
        display: inline-block !important;
        width: 58.33333% !important;
      }
      td.small-8,
      th.small-8 {
        display: inline-block !important;
        width: 66.66667% !important;
      }
      td.small-9,
      th.small-9 {
        display: inline-block !important;
        width: 75%;
      }
      td.small-10,
      th.small-10 {
        display: inline-block !important;
        width: 83.33333% !important;
      }
      td.small-11,
      th.small-11 {
        display: inline-block !important;
        width: 91.66667% !important;
      }
      td.small-12,
      th.small-12 {
        display: inline-block !important;
        width: 100% !important;
      }
      .columns td.small-12,
      .column td.small-12,
      .columns th.small-12,
      .column th.small-12 {
        display: block !important;
        width: 100% !important;
      }
      table.body td.small-offset-1,
      table.body th.small-offset-1 {
        margin-left: 8.33333% !important;
        margin-left: 8.33333% !important;
      }
      table.body td.small-offset-2,
      table.body th.small-offset-2 {
        margin-left: 16.66667% !important;
        margin-left: 16.66667% !important;
      }
      table.body td.small-offset-3,
      table.body th.small-offset-3 {
        margin-left: 25% !important;
        margin-left: 25% !important;
      }
      table.body td.small-offset-4,
      table.body th.small-offset-4 {
        margin-left: 33.33333% !important;
        margin-left: 33.33333% !important;
      }
      table.body td.small-offset-5,
      table.body th.small-offset-5 {
        margin-left: 41.66667% !important;
        margin-left: 41.66667% !important;
      }
      table.body td.small-offset-6,
      table.body th.small-offset-6 {
        margin-left: 50% !important;
        margin-left: 50% !important;
      }
      table.body td.small-offset-7,
      table.body th.small-offset-7 {
        margin-left: 58.33333% !important;
        margin-left: 58.33333% !important;
      }
      table.body td.small-offset-8,
      table.body th.small-offset-8 {
        margin-left: 66.66667% !important;
        margin-left: 66.66667% !important;
      }
      table.body td.small-offset-9,
      table.body th.small-offset-9 {
        margin-left: 75% !important;
        margin-left: 75% !important;
      }
      table.body td.small-offset-10,
      table.body th.small-offset-10 {
        margin-left: 83.33333% !important;
        margin-left: 83.33333% !important;
      }
      table.body td.small-offset-11,
      table.body th.small-offset-11 {
        margin-left: 91.66667% !important;
        margin-left: 91.66667% !important;
      }
      table.body table.columns td.expander,
      table.body table.columns th.expander {
        display: none !important;
      }
      table.body .right-text-pad,
      table.body .text-pad-right {
        padding-left: 10px !important;
      }
      table.body .left-text-pad,
      table.body .text-pad-left {
        padding-right: 10px !important;
      }
      table.menu {
        width: 100% !important;
      }
      table.menu td,
      table.menu th {
        width: auto !important;
        display: inline-block !important;
      }
      table.menu.vertical td,
      table.menu.vertical th,
      table.menu.small-vertical td,
      table.menu.small-vertical th {
        display: block !important;
      }
      table.menu[align="center"] {
        width: auto !important;
      }
      table.button.small-expand,
      table.button.small-expanded {
        width: 100% !important;
      }
      table.button.small-expand table,
      table.button.small-expanded table {
        width: 100%;
      }
      table.button.small-expand table a,
      table.button.small-expanded table a {
        text-align: center !important;
        width: 100% !important;
        padding-left: 0 !important;
        padding-right: 0 !important;
      }
      table.button.small-expand center,
      table.button.small-expanded center {
        min-width: 0;
      }
    }
  .pink-row{width: 12%;position: relative;}
  .pink-row:after{content: '';border-bottom: 1px dotted #ed1864;position: absolute;left: -33px;top: 26px;width: 218%;}
</style>
<!-- <style> -->
<table class="body" data-made-with-foundation="" style="margin: 0; border-collapse: collapse; border-spacing: 0; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; height: 100%; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
    <tbody>
        <tr style="padding: 0; text-align: left; vertical-align: top;">
            <td align="center" class="float-center" style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0 auto; border-collapse: collapse !important; color: #0a0a0a; float: none; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0 auto; padding: 0; text-align: center; vertical-align: top; word-wrap: break-word;" valign="top">
              <center data-parsed="" style="min-width: 580px; width: 100%;">
                <table align="center" class="container float-center" style="margin: 0 auto; border-collapse: collapse; border-spacing: 0; float: none; margin: 0 auto; padding: 0; text-align: center; vertical-align: top; width: 580px;">
                    <tbody>
                        <tr style="padding: 0; text-align: left; vertical-align: top;">
                          <td style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 22px; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;box-shadow: 0px 0px 8px #ccc;background-color: #fefefe;">
                            <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
                                <tbody>
                                    <tr class="mail-header" style="padding: 0; text-align: left; vertical-align:top;">
                                      <th>
                                        <div>
                                          <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                                              <tbody>                                            
                                                  <tr style="padding: 0; text-align: left; vertical-align: top;">
                                                      <th style="line-height: 1.3; margin: 0; padding: 25px 0; text-align: left;">
                                                        <center>
                                                          <span class="sg-image" data-imagelibrary="%7B%22width%22%3A%22140%22%2C%22height%22%3A%2242%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//duweqcg9u3g3s.cloudfront.net/emails/Intrwiz+Registration/LOGO_Intrwiz_ArrivalGuides_Darkbg.png%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D">
                                                            <img id="logo" src="img/logo.png" style="clear: both;max-width: 100%;outline: none;text-decoration: none;width: 200px;height: auto;" />
                                                          </span>
                                                        </center>                                                
                                                      </th>
                                                  </tr>
                                                  <tr>
                                                      <th style="color: #0a0a0a; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0; padding: 64px 0; text-align: center; background-image: url('img/email-temp-banner-img.jpg');">
                                                      <h1 class="text-center" style="Margin: 0;Margin-bottom: 10px;color: #fff;font-family: Helvetica, Arial, sans-serif;font-size: 34px;font-weight: bold;line-height: 1.3;margin: 0;margin-bottom: 10px;padding: 0;text-align: center;word-wrap: normal;">Welcome!<br/> <span style="color:#fff;font-weight: 400;">You are Registered</span></h1>
                                                      </th>
                                                  </tr>
                                              </tbody>
                                          </table>
                                        </div>
                                      </th>
                                    </tr>

                                    <tr>
                                        <th style="font-family: Helvetica, Arial, sans-serif; font-size: 15px;color: #5e5e5e;  font-weight: normal; line-height: 1.3;border-bottom: 1px solid #d9d9d9; margin: 0; margin: 0; padding: 35px; text-align: left;">
                                            <p style="padding: 0;margin: 20px 0;"><span style="font-size: 16px;font-weight: 700;">Dear [COMPANY NAME]</span>,</p>
                                            <p style="padding: 0;margin: 16px 0;"> We are proud to introduce the most sophisticated travel platform in the world specially designed by industry experts to give travel agents a decisive advantage in the marketplace.</p>
                                            <p style="padding: 0;margin: 20px 0;">The platform provides you with a turn key branded platform help you effectively compete with Online Travel Providers and win!</p>
                                            <p style="padding: 0;margin: 20px 0;">If you have requested to set up a line of credit or floting deposite, we will be in touch with you to set this up.</p>
                                            <p style="padding: 0;margin: 20px 0;">Your Intrwiz team.</p>
                                          </p>
                                        </th>
                                    </tr>
                                    <tr>
                                      <td style="border-bottom: 1px solid #d9d9d9;padding: 50px;margin: 0;">
                                      <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                          <td>
                                            <h2 style="font-size: 18px;font-weight: 700;color: #5e5e5e;padding: 0;margin: 0 0 6px;text-align: center;">There are 3 credentials to your components</h2>
                                            <p style="font-size: 15px;color: #5e5e5e;padding: 0;margin: 0 0 25px;text-align: center;">that will help you drive sales and grow your business</p>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td>
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                              <tr>
                                                <td style="font-size: 14px;color: #9b9b9b;text-align: center;line-height: 18px;vertical-align: top;width: 25%;">
                                                  <p style="padding: 0;margin: 0 0 15px;"><img src="img/icon-client-facing-portal.jpg"></p>
                                                  <p>Your</br> Client-facing</br> Portal</p>
                                                </td>
                                                <td style="vertical-align: top;" class="pink-row"></td>
                                                <td style="font-size: 14px;color: #9b9b9b;text-align: center;line-height: 18px;vertical-align: top;width: 25%;">
                                                  <p style="padding: 0;margin: 0 0 15px;"><img src="img/icon-agent-dashboard.jpg"></p>
                                                  <p>Your</br> Agent Dashboard</p>
                                                </td>
                                                <td style="vertical-align: top;" class="pink-row"></td>
                                                <td style="font-size: 14px;color: #9b9b9b;text-align: center;line-height: 18px;vertical-align: top;width: 25%;">
                                                  <p style="padding: 0;margin: 0 0 15px;"><img src="img/icon-your-control-panel.jpg"></p>
                                                  <p>Your</br> Control Panel</p>
                                                </td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td style="border-bottom: 1px solid #d9d9d9;">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                          <tr>
                                            <td style="text-align: center; padding: 35px 0 0;margin: 0;"><img src="img/icon-paper-plane.png" style="clear: both;max-width: 100%;outline: none;text-decoration: none;width: 45px;height: auto;"></td>
                                          </tr>
                                          <tr>
                                            <td style="text-align: center;"><h3 style="font-size: 18px;font-weight: 700;color: #41838b;padding: 0;margin: 15px 0 25px;">Your Client Facing Portal</h3></td>
                                          </tr>
                                          <tr>
                                            <td style="text-align: center; padding: 0 35px 35px;font-size: 15px;color: #5e5e5e;">
                                              <p style="padding: 0;margin: 0 0 25px 0;">State of the Art Collaborative Interactive Booking Site fully enabled with global direct content connected to hundreds of suppliers.</p>

                                              <p style="padding: 0;margin: 25px 0;">Each search automatically creates a Branded Interactive Micro Site directly connected to your Agent Dashboard allowing you to seamlessly monitor and engage in real time with each customer.</p>
                                              <p style="padding: 0;margin: 50px 0 40px;">
                                                <a href="#" style="height: 20px; width: auto; font-size: 15px; line-height: 20px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; padding: 12px 40px; text-decoration: none;text-decoration: none; -webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px;background-color: #ed1864; display: inline-block; font-weight: 400;" target="_blank"><b>Take Me There</b></a>
                                              </p>
                                              <p style="padding: 0;margin: 0 0 15px; text-align: center;font-size: 15px;font-weight: 600;">
                                                Client Facing Url: <span><a style="font-weight: 400;color: #ed1864;text-decoration: none;" href="#">http://abctraveles1.takemethere.ca</a></span>
                                              </p>
                                            </td>
                                          </tr>                                      
                                        </table>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td style="border-bottom: 1px solid #d9d9d9;">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                          <tr>
                                            <td style="text-align: center; padding: 35px 0 0;margin: 0;"><img src="img/icon-dashboard.png" style="clear: both;max-width: 100%;outline: none;text-decoration: none;width: 45px;height: auto;"></td>
                                          </tr>
                                          <tr>
                                            <td style="text-align: center;"><h3 style="font-size: 18px;font-weight: 700;color: #41838b;padding: 0;margin: 15px 0 25px;">Agent Dashboard</h3></td>
                                          </tr>
                                          <tr>
                                            <td style="text-align: center; padding: 0 35px 35px;font-size: 15px;color: #5e5e5e;">
                                              <p style="padding: 0;margin: 0 0 25px 0;">Each Branded Interactive Customer Micro Site created by you, your customer or via your GDS is connected to your agent dashboard.</p>
                                              <p style="padding: 0;margin: 25px 0;">You can interact with each of your customers in real time provide a unique end to end experience that will keep your customers coming back to you forever.</p>
                                              <p style="padding: 0;margin: 25px 0;">Add your own content to the trip or leverage Intrwiz content allowing you to maximize your commissions including 24/7 support for you and your customers.</p>
                                              <p style="font-size: 15px;font-weight: 700;color: #5e5e5e; text-align: center;padding: 0;margin: 0;">Username:</p>
                                              <p style="text-align: center;padding: 0;margin: 5px 0 15px;"><a  style="font-size: 15px;font-weight: 700;color: #ed1864;text-decoration: none;" href="#">someperson@snowstormtech.com</a></p>
                                              <p style="font-size: 15px;font-weight: 700;color: #5e5e5e; text-align: center;padding: 0;margin: 0;">Password:</p>
                                              <p style="text-align: center;padding: 0;margin: 5px 0 0;font-size: 15px;font-weight: 400;color: #5e5e5e;">12345678</p>
                                              <p style="padding: 0;margin: 20px 0 30px;">
                                                <a href="#" style="height: 20px; width: auto; font-size: 15px; line-height: 20px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; padding: 12px 40px; text-decoration: none;text-decoration: none; -webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px;background-color: #ed1864; display: inline-block; font-weight: 400;" target="_blank"><b>Take Me There</b></a>
                                              </p>
                                              <p style="padding: 0;margin: 0 0 15px; text-align: center;font-size: 15px;font-weight: 600;">
                                                Dashboard Url: <span><a style="font-weight: 400;color: #ed1864;text-decoration: none;" href="#">http://abctraveles1.takemethere.ca</a></span>
                                              </p>
                                            </td>
                                          </tr>                                      
                                        </table>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td style="border-bottom: 1px solid #d9d9d9;">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                          <tr>
                                            <td style="text-align: center; padding: 35px 0 0;margin: 0;"><img src="img/icon-control-panel.png" style="clear: both;max-width: 100%;outline: none;text-decoration: none;width: 45px;height: auto;"></td>
                                          </tr>
                                          <tr>
                                            <td style="text-align: center;"><h3 style="font-size: 18px;font-weight: 700;color: #41838b;padding: 0;margin: 15px 0 25px;">Your Control Panel</h3></td>
                                          </tr>
                                          <tr>
                                            <td style="text-align: center; padding: 0 35px 35px;font-size: 15px;color: #5e5e5e;">
                                              <p style="padding: 0;margin: 0 0 25px 0;">Customize your client facing portal and each Branded Interactive Customer Micro Site including look and feel, messaging, suppliers, booking engines, pricing, email notifications and many other intuitive features to help you create a unique branded experience for your customers.</p>
                                              <p style="font-size: 15px;font-weight: 700;color: #5e5e5e; text-align: center;padding: 0;margin: 0;">Username:</p>
                                              <p style="text-align: center;padding: 0;margin: 5px 0 15px;"><a  style="font-size: 15px;font-weight: 700;color: #ed1864;text-decoration: none;" href="#">someperson@snowstormtech.com</a></p>
                                              <p style="font-size: 15px;font-weight: 700;color: #5e5e5e; text-align: center;padding: 0;margin: 0;">Password:</p>
                                              <p style="text-align: center;padding: 0;margin: 5px 0 0;font-size: 15px;font-weight: 400;color: #5e5e5e;">12345678</p>
                                              <p style="padding: 0;margin: 20px 0 30px;">
                                                <a href="#" style="height: 20px; width: auto; font-size: 15px; line-height: 20px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; padding: 12px 40px; text-decoration: none;text-decoration: none; -webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px;background-color: #ed1864; display: inline-block; font-weight: 400;" target="_blank"><b>Take Me There</b></a>
                                              </p>
                                              <p style="padding: 0;margin: 0 0 15px; text-align: center;font-size: 15px;font-weight: 600;">
                                                Control Panel Url: <span><a style="font-weight: 400;color: #ed1864;text-decoration: none;" href="#">http://abctraveles1.takemethere.ca</a></span>
                                              </p>
                                            </td>
                                          </tr>                                      
                                        </table>
                                      </td>
                                    </tr>
                                </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td style="padding: 50px 50px 10px;margin: 0;font-size: 15px;color: #3b3b3b;">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                              <tr>
                                <td style="font-size: 15px;font-weight: 700;color: #3b3b3b;padding: 0;margin: 0;">Contact Us</td>
                              </tr>
                              <tr>
                                <td style="padding: 25px 0 35px;margin: 0;"><span style="font-weight: 400;font-style: italic;">Email:</span> <a href="#" style="font-weight: 600;text-decoration: underline;color: #5e5e5e;">support@intrwiz.com</a></td>
                              </tr>
                              <tr>
                                <td style="padding: 15px 0;margin: 0;font-size: 15px;color: #5e5e5e;text-align: left;">
                                  <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                      <td style="font-weight: 700;color: #5e5e5e;padding: 6px 0;margin: 0;">USA</td>
                                      <td style="padding: 6px 0;margin: 0;"><a href="#" style="font-size: 15px;font-weight: 500;text-decoration: underline;color: #5e5e5e;">+1 -(800)- 799-4451</a></td>
                                      <td style="font-weight: 700;color: #5e5e5e;padding: 6px 0;margin: 0;">Pakistan</td>
                                      <td style="padding: 6px 0;margin: 0;"><a href="#" style="font-size: 15px;font-weight: 500;text-decoration: underline;color: #5e5e5e;">1 -123-123-1234</a></td>
                                    </tr>
                                    <tr>
                                      <td style="font-weight: 700;color: #5e5e5e;padding: 6px 0;margin: 0;">Canada</td>
                                      <td style="padding: 6px 0;margin: 0;"><a href="#" style="font-size: 15px;font-weight: 500;text-decoration: underline;color: #5e5e5e;">+1 -(647)- 799-0250</a></td>
                                      <td style="font-weight: 700;color: #5e5e5e;padding: 6px 0;margin: 0;">Kenya</td>
                                      <td style="padding: 6px 0;margin: 0;"><a href="#" style="font-size: 15px;font-weight: 500;text-decoration: underline;color: #5e5e5e;">1 -123-123-1234</a></td>
                                    </tr>
                                    <tr>
                                      <td style="font-weight: 700;color: #5e5e5e;padding: 6px 0;margin: 0;">Mexico</td>
                                      <td style="padding: 6px 0;margin: 0;"><a href="#" style="font-size: 15px;font-weight: 500;text-decoration: underline;color: #5e5e5e;">+52 -(55)- 85261991</a></td>
                                      <td style="font-weight: 700;color: #5e5e5e;padding: 6px 0;margin: 0;">Tanzani</td>
                                      <td style="padding: 6px 0;margin: 0;"><a href="#" style="font-size: 15px;font-weight: 500;text-decoration: underline;color: #5e5e5e;">1 -123-123-1234</a></td>
                                    </tr>
                                    <tr>
                                      <td style="font-weight: 700;color: #5e5e5e;padding: 6px 0;margin: 0;">London</td>
                                      <td style="padding: 6px 0;margin: 0;"><a href="#" style="font-size: 15px;font-weight: 500;text-decoration: underline;color: #5e5e5e;">+44 -(20)- 83966666</a></td>
                                      <td style="font-weight: 700;color: #5e5e5e;padding: 6px 0;margin: 0;">Uganda</td>
                                      <td style="padding: 6px 0;margin: 0;"><a href="#" style="font-size: 15px;font-weight: 500;text-decoration: underline;color: #5e5e5e;">1 -123-123-1234</a></td>
                                    </tr>
                                    <tr>
                                      <td style="font-weight: 700;color: #5e5e5e;padding: 6px 0;margin: 0;">Kuwait</td>
                                      <td style="padding: 6px 0;margin: 0;"><a href="#" style="font-size: 15px;font-weight: 500;text-decoration: underline;color: #5e5e5e;">1 -123-123-1234</a></td>
                                      <td style="font-weight: 700;color: #5e5e5e;padding: 6px 0;margin: 0;">Uganda</td>
                                      <td style="padding: 6px 0;margin: 0;"><a href="#" style="font-size: 15px;font-weight: 500;text-decoration: underline;color: #5e5e5e;">1 -123-123-1234</a></td>
                                    </tr>
                                    <tr>
                                      <td style="font-weight: 700;color: #5e5e5e;padding: 6px 0;margin: 0;">Sweden</td>
                                      <td style="padding: 6px 0;margin: 0;"><a href="#" style="font-size: 15px;font-weight: 500;text-decoration: underline;color: #5e5e5e;">1 -123-123-1234</a></td>
                                      <td style="font-weight: 700;color: #5e5e5e;padding: 6px 0;margin: 0;">New Zealand</td>
                                      <td style="padding: 6px 0;margin: 0;"><a href="#" style="font-size: 15px;font-weight: 500;text-decoration: underline;color: #5e5e5e;">1 -123-123-1234</a></td>
                                    </tr>
                                    <tr>
                                      <td style="font-weight: 700;color: #5e5e5e;padding: 6px 0;margin: 0;">UAE</td>
                                      <td style="padding: 6px 0;margin: 0;"><a href="#" style="font-size: 15px;font-weight: 500;text-decoration: underline;color: #5e5e5e;">1 -123-123-1234</a></td>
                                      <td style="font-weight: 700;color: #5e5e5e;padding: 6px 0;margin: 0;">&nbsp;</td>
                                      <td style="padding: 6px 0;margin: 0;">&nbsp;</td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                    </tbody>
                </table>
              </center>
            </td>
        </tr>        
    </tbody>
</table>
</body>
</html>
<?php

include_once 'config.php';
include_once 'model/ajaxprocessModel.php';
//print_r($_REQUEST);
$data = [];
$data = $_REQUEST;
switch ($data["action"]) {
    case "register":
        // $result = $this->callTmtApiHandler($api_function, self::CALL_TYPE_POST, $request, [], false, false, 'application/json; charset=utf-8', false);
        // callTmtApiHandler($function_url, $call_type, $request_params = array(), array $url_params = array(), $return_raw = false, $debug = false, $contentType = 'application/json; charset=utf-8', $requeireAuth = true) ;
       
        $request_params = [
            'CompanyName' => $data['CompanyName'],
            'FirstName' => $data['FirstName'],
            'LastName' => $data['LastName'],
            'EmailID' => $data['email'],
            'ContactNo' => $data['ContactNumber'],
            'Password' => $data['password'],
            'DomainAlias' => str_replace(" ", "-", trim($data['CompanyName'])),
            'CompanyTheme' => 'jumbo',
            'ACTbannertoshow' => false,
            'CompanyCountry'=>!empty($data['CompanyCountry']) ? $data['CompanyCountry'] : '',
            'CompanyCurrency'=>!empty($data['CompanyCurrency']) ? $data['CompanyCurrency'] : '',
        ];

        //var_dump($data["preferredPaymentMode"]);




        $function_url = API_END_POINT.'/Admin/api/v2/agentautoregister';
        $call_type = 'POST';

        $contentType = 'application/json; charset=utf-8';
        $url_params = [];
        $debug = FALSE;

        $ajaxprocessModel = new ajaxprocessModel();
        $response = $ajaxprocessModel->register($function_url, $call_type, $request_params, $url_params, $contentType, $debug);
        

        if ($response["code"] == 200) {
            foreach ($data["preferredPaymentMode"] as $v) {
                $subject = $msg = "";
                //echo $v." ->".$response['details']['CompanyWebSite'];
                switch ($v) {
                    
                    case "floating_deposit":
                        $subject = "Request for Floating Deposit Payment Method by " . $data['FirstName'];

                        $msg = generatePaymentMethodRequestHtml($data, "The following Agent has registered and requested for <b>  Floating Deposit Payment Option. </b> <br>
                                        You can approve this from control panel", 'http://' . $response['details']['CompanyWebSite']);
                        break;

                    case "line_of_credit":

                        $subject = "Request for Line of Credit Payment Method by " . $data['FirstName'];

                        $msg = generatePaymentMethodRequestHtml($data, "The following Agent has registered and requested for <b>  Line of Credit Payment Option. </b> <br>
                                        You can approve this from control panel", 'http://' . $response['details']['CompanyWebSite']);

                        break;
                }

                if ($subject != "" && $msg != "") {
                    
                    $message_type = 32;
                    $substitution_tags['<%subject%>'] = $subject;
                    $substitution_tags['<%message_body%>'] = $msg;
                    $substitution_tags = appendDefaultSubstitutionTages($substitution_tags);
                    
                    $response["email_snd"][$subject] = $ajaxprocessModel->email($substitution_tags, PAYMENT_METHOD_NOTIFICATION_EMAIL, $message_type, DEFAULT_EMAIL_FROM_NAME, DEFAULT_COMPANY_ID);
                }
                //var_dump($v);
            }
        }




        echo json_encode($response);
        break;

    case 'contactUs':

        $html = "<table style='text-align:left'>";

//        $html .= "<tr>";
//        $html .= "<th style='width:40%'>Name </th>";
//        $html .= "<td> " . $data['first_name'] ." ".  $data['last_name']." </td>";
//        $html .= "</tr>";
//        
//        $html .= "<tr>";
//        $html .= "<th>Email </th>";
//        $html .= "<td> " . $data['email'] ." </td>";
//        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<th>Message : </th>";
        $html .= "<td> " . $data['message'] . " </td>";
        $html .= "</tr>";

        $html .= "<table>";

        $emailAddress = "aranyak.banerjee@dreamztech.com";
        $message_type = CONTACT_US_TEMPLATE;
        $substitution_tags['<%subject%>'] = 'New Contact';
        $substitution_tags['<%user_First_name%>'] = $data['first_name'];
        $substitution_tags['<%user_Last_name%>'] = $data['last_name'];
        $substitution_tags['<%email_address%>'] = $data['email'];
        $substitution_tags['<%message%>'] = $html;
        $substitution_tags = appendDefaultSubstitutionTages($substitution_tags);

        $ajaxprocessModel = new ajaxprocessModel();

        $response = $ajaxprocessModel->email($substitution_tags, CONTACT_EMAIL, $message_type, DEFAULT_EMAIL_FROM_NAME, DEFAULT_COMPANY_ID);
        echo json_encode(["msg" => "", "status" => $response]);
        break;
    case 'carrier':
        $data['phone'] = !empty($data['phone']) ? $data['phone'] : "NA";

        $html = "<table style='text-align:left'>";

//        $html .= "<tr>";
//        $html .= "<th style='width:40%'>Name </th>";
//        $html .= "<td> " . $data['first_name'] ." ".  $data['last_name']." </td>";
//        $html .= "</tr>";
//        
//        $html .= "<tr>";
//        $html .= "<th>Email </th>";
//        $html .= "<td> " . $data['email'] ." </td>";
//        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<th> Phone Number : </th>";
        $html .= "<td> " . $data['phone'] . " </td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<th>Message : </th>";
        $html .= "<td> " . $data['message'] . " </td>";
        $html .= "</tr>";

        $html .= "<table>";


        $emailAddress = "aranyak.banerjee@dreamztech.com";
        $message_type = CONTACT_US_TEMPLATE;
        $substitution_tags['<%subject%>'] = 'Carrier';
        $substitution_tags['<%user_First_name%>'] = $data['first_name'];
        $substitution_tags['<%user_Last_name%>'] = $data['last_name'];
        $substitution_tags['<%email_address%>'] = $data['email'];
        $substitution_tags['<%message%>'] = $html;
        $substitution_tags = appendDefaultSubstitutionTages($substitution_tags);
        $ajaxprocessModel = new ajaxprocessModel();

        $response = $ajaxprocessModel->email($substitution_tags, CONTACT_EMAIL, $message_type, DEFAULT_EMAIL_FROM_NAME, DEFAULT_COMPANY_ID);
        echo json_encode(["msg" => "", "status" => $response]);
        break;
    case 'affiliate':
        $data['phone'] = !empty($data['phone']) ? $data['phone'] : "NA";
        $html = "<table style='text-align:left'>";

//        $html .= "<tr>";
//        $html .= "<th style='width:40%'>Name </th>";
//        $html .= "<td> " . $data['first_name'] ." ".  $data['last_name']." </td>";
//        $html .= "</tr>";
//        
//        $html .= "<tr>";
//        $html .= "<th>Email </th>";
//        $html .= "<td> " . $data['email'] ." </td>";
//        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<th> Phone Number : </th>";
        $html .= "<td> " . $data['phone'] . " </td>";
        $html .= "</tr>";

        $html .= "<tr>";
        $html .= "<th>Message : </th>";
        $html .= "<td> " . $data['message'] . " </td>";
        $html .= "</tr>";

        $html .= "<table>";



        $message_type = CONTACT_US_TEMPLATE;
        $substitution_tags['<%subject%>'] = 'Affiliate';
        $substitution_tags['<%user_First_name%>'] = $data['first_name'];
        $substitution_tags['<%user_Last_name%>'] = $data['last_name'];
        $substitution_tags['<%email_address%>'] = $data['email'];
        $substitution_tags['<%message%>'] = $html;
        $substitution_tags = appendDefaultSubstitutionTages($substitution_tags);

        $ajaxprocessModel = new ajaxprocessModel();

        $response = $ajaxprocessModel->email($substitution_tags, CONTACT_EMAIL, $message_type, DEFAULT_EMAIL_FROM_NAME, DEFAULT_COMPANY_ID);
        echo json_encode(["msg" => "", "status" => $response]);
        break;

    default:
        break;
}

function appendDefaultSubstitutionTages($substitution_tags) {
    $substitution_tags['<%company_name%>'] = DEFAULT_EMAIL_FROM_NAME;
    $substitution_tags['<%company_logo%>'] = DEFAULT_COMPANY_LOGO;
    return $substitution_tags;
}

function generatePaymentMethodRequestHtml($data, $messageLine, $companyUrl) {
    $html = "<table style='text-align:left;'>
                                <tr>
                                    <td>
                                        Hello Admin
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        " . $messageLine . "
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <th>
                                                    Company Name
                                                </th>
                                                <td>
                                                
                                                " . $data['CompanyName'] . "
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    Company Url
                                                </th>
                                                <td>
                                                
                                                " . $companyUrl . "
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    Agent Name
                                                </th>
                                                <td>
                                                " . $data['FirstName'] . " " . $data['LastName'] . "

                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    Agent Email
                                                </th>
                                                <td>
                                                    " . $data['email'] . "

                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    Agent Phone number
                                                </th>
                                                <td>
                                                
                                                     " . $data['ContactNumber'] . "
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                </table>";
    return $html;
}

?>
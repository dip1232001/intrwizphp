<?php include_once 'partials/head.php'; ?>
<?php include_once 'partials/navbar.php'; ?>

<!-- Body Section Start -->
<div id="global-partners-body-wrap">
    <div class="global-partners">
        <div class="container">

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="left-content-panel">
                        <h2>Global Distribution Partners</h2>
                        <p>Intrwiz is appointing global distribution partners to deploy the Intrwiz suite of products as part of our overall global strategy. If you are interested in a particular region, please Contact Us.</p>
                        <a href="javascript:void(0)" onclick="$('#modal__contact').modal('show');" class="red-button">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body Section End -->

<?php include_once 'partials/footer.php'; ?>

</body>
</html>

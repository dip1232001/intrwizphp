<?php
include_once 'config.php';
require_once 'model/Mobile_Detect.php';
$mobiledetect = new Mobile_Detect();
//var_dump($_SERVER['REQUEST_URI']);
//exit;
if(empty($_SERVER['REQUEST_URI']) || $_SERVER['REQUEST_URI'] == '/' || $_SERVER['REQUEST_URI'] == '/mobile' || $_SERVER['REQUEST_URI'] == '/mobile/'){
    $_SERVER['REQUEST_URI'] = '/index.php';
}
//var_dump($mobiledetect);
//exit;
if ($mobiledetect->isMobile() && !$mobiledetect->isTablet()) {
    header("Location: /mobile" . $_SERVER['REQUEST_URI']);
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php
        //  echo '<pre>';
        //print_r($_SERVER);
        $pagename = ltrim($_SERVER['REQUEST_URI'], '/');
        // exit;
        if ($pagename == '' || $pagename == 'index.php') {
            $title = 'intrwiz';
        } elseif ($pagename == 'agent-platform.php') {
            $title = 'Agent Platform';
        } elseif ($pagename == 'using-intrwiz.php') {
            $title = 'Using Intrwiz';
        } elseif ($pagename == 'pricing.php') {
            $title = 'Pricing';
        } elseif ($pagename == 'global-partners.php') {
            $title = 'Global Partners';
        } elseif ($pagename == 'press.php') {
            $title = 'News';
        } elseif ($pagename == 'affinity-site.php') {
            $title = 'Closed Group Affinity Site';
        } elseif ($pagename == 'full-end-to-end-custom.php') {
            $title = 'End to End custom Booking Site';
        }
        ?>

        <title><?php echo $title; ?></title>

        <!-- Bootstrap Core CSS -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/custom.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link rel="shortcut icon" href="img/favicon.ico">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- Script to Activate the Carousel -->    

        <script type="text/javascript">
            $(document).ready(function () {
                $('#myCarousel').carousel({
                    interval: 10000
                })
            });
        </script>

    </head>

    <body>
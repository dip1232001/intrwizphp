<!-- Footer Section Start -->
<?php include_once 'partials/modalelement.php'; ?>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-12">
                <img  src="img/footer-logo.png" class="img-responsive footerLogo">
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">

                <h3 class="hide">Follow Us</h3>
                <ul class="social-icons hide">
                    <li class="hide"><a href="javascript:void(0)"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li class="hide"><a href="javascript:void(0)"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="javascript:void(0)"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12"></div>
            <div class="col-lg-3 col-md-3 col-sm-12">
                <h3>Quick Links</h3>
                <ul>
                    <li><a href="agent-platform.php">Agent Platform</a></li>
                    <li><a href="affinity-site.php">Closed User Group Affinity Sites</a></li>
                    <li><a href="full-end-to-end-custom.php">Full End to End Online Booking Platforms</a></li>
                    <li>
                        <a href="collaborative-sollution.php">Collaborative Itinerary Solutions</a>
                    </li>
                    <!--<li><a href="pricing.php">Pricing</a></li>-->
                    <li><a href="global-partners.php">Global Partners</a></li>
                   
                    
                    <!--<li><a href="javascript:void(0)">Resources</a></li>-->
                    <li><a href="press.php">News</a></li>
                    <li><a href="javascript:void(0)" onclick="$('#modal__contact').modal('show');">Contact Us</a></li>
                    <li><a href="javascript:void(0)" onclick="$('#modal__careers').modal('show');">Career Inquiries</a></li>
                    <li><a href="javascript:void(0)" onclick="$('#modal__affiliate').modal('show');">Affiliate Inquiries</a></li>
                </ul>
            </div>
            
        </div>
    </div>
</footer>

<!-- Footer Section End -->


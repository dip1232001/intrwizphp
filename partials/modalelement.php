<style>
    .form-holder .gform_wrapper textarea.error, .form-holder .gform_wrapper input.error {
        border: 1px solid red !important;
    }
</style>
<div id="modal-cover">
    <div id="modal__contact" class="modal " style="display: none;">
        <div class="modal__content">
            <div class="modal__close"><i class="icon-close">X</i></div>
            <div id="" class="modal_body">
                <h2 style="text-align: left;">Contact Us</h2>
                <p>&nbsp;</p>
<!--                <p><b>Toll Free Number:</b> (888)-737-8902</p>
                <p><b>Outside Canada or USA:</b> (647)-360-7477</p>-->
                <?php include_once 'contactnumberpartial.php';?>
                <p style="text-align: left;">We are here to answer any question you may have about our products. Please fill out the contact form and we will respond as soon as we can.</p>
                <div id="submitsuccess_contact" class="alert alert-success" style="display:none;">

                </div>
                <div id="submiterror_contact" class="alert alert-danger" style="display:none;">

                </div>
                <div class="form-holder">
                    <div class="gf_browser_gecko gform_wrapper" id="gform_wrapper_28">

                        <form id="contactUsForm">
                            <div class="gform_body">
                                <input type="hidden" value="contactUs" name="action">

                                <ul id="" class="gform_fields top_label form_sublabel_below description_below">
                                    <li id="" class="gfield medium-6 columns gfield_contains_required field_sublabel_below field_description_below">
                                        <label class="gfield_label" for="">First Name<span class="gfield_required">*</span></label>
                                        <div class="ginput_container ginput_container_text">
                                            <input name="first_name" id="first_name" type="text" value="" required class="medium" tabindex="1" placeholder="Enter First Name">
                                        </div>
                                    </li>
                                    <li id="" class="gfield medium-6 columns gfield_contains_required field_sublabel_below field_description_below">
                                        <label class="gfield_label" for="">Last Name<span class="gfield_required">*</span></label>
                                        <div class="ginput_container ginput_container_text">
                                            <input name="last_name" id="last_name" type="text" value="" required class="medium" tabindex="2" placeholder="Enter Last Name">
                                        </div>
                                    </li>
                                    <li id="" class="gfield gfield_contains_required field_sublabel_below field_description_below">
                                        <label class="gfield_label" for="">Email<span class="gfield_required">*</span></label>
                                        <div class="ginput_container ginput_container_email">
                                            <input name="email" id="email" type="text" value="" required class="large" tabindex="3" placeholder="Ex. name@company.com">
                                        </div>
                                    </li>
                                    <li id="" class="gfield gfield_contains_required field_sublabel_below field_description_below">
                                        <label class="gfield_label" for="">Message<span class="gfield_required">*</span></label>
                                        <div class="ginput_container ginput_container_textarea">
                                            <textarea name="message" id="message" required class="textarea large" tabindex="4" placeholder="Enter a message" rows="10" cols="50"></textarea>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="modal-btn-submit"> 
                                <button type="button" class="btn btn-lg btn-pink" onclick="submitContactUs(event)" data-loading-text="Contact <i class='fa fa-circle-o-notch fa-spin'></i> ">
                                    Contact
                                </button>
                            </div>

                        </form>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <div id="modal__terms" class="modal " style="display: none;">
        <div class="modal__content">
            <div class="modal__close"><i class="icon-close">X</i></div>
            <div id=""><h2 style="text-align: left;">Terms &amp; Conditions</h2>
                <p style="text-align: left;">Coming soon.</p>
            </div>
        </div>
    </div>
    <div id="modal__careers" class="modal " style="display: none;">
        <div class="modal__content">
            <div class="modal__close"><i class="icon-close">X</i></div>
            <div id="" class="modal_body">
                <h2 style="text-align: left;">Career Inquiries</h2>
                <p>We are always looking to meet talented, passionate team players who are natural thinkers and doers. Please fill out the form below if you are looking for an exciting new role in <strong>technology, design, sales or marketing.</strong><br>
                    Our HR team will get in touch with you with potential opportunities.</p>
                <div id="submitsuccess_career" class="alert alert-success" style="display:none;">

                </div>
                <div id="submiterror_career" class="alert alert-danger" style="display:none;">

                </div>
                <div class="form-holder">
                    <div class="gf_browser_gecko gform_wrapper" id="gform_wrapper_29">
                        <a id="" class="gform_anchor"></a>
                        <form  id="careerForm" >
                            <input type="hidden" name="action" value="carrier">

                            <div class="gform_body">
                                <ul id="" class="gform_fields top_label form_sublabel_below description_below">
                                    <li id="" class="gfield medium-6 columns gfield_contains_required field_sublabel_below field_description_below">
                                        <label class="gfield_label" for="first_name">First Name<span class="gfield_required">*</span></label>
                                        <div class="ginput_container ginput_container_text">
                                            <input name="first_name" id="first_name" type="text" value="" class="medium" tabindex="1" placeholder="Enter First Name">
                                        </div>
                                    </li>
                                    <li id="field_29_7" class="gfield medium-6 columns gfield_contains_required field_sublabel_below field_description_below">
                                        <label class="gfield_label" for="last_name">Last Name<span class="gfield_required">*</span></label>
                                        <div class="ginput_container ginput_container_text">
                                            <input name="last_name" id="last_name" type="text" value="" class="medium" tabindex="2" placeholder="Enter Last Name">
                                        </div>
                                    </li>
                                    <li id="field_29_1" class="gfield gfield_contains_required field_sublabel_below field_description_below">
                                        <label class="gfield_label" for="email">Email<span class="gfield_required">*</span></label>
                                        <div class="ginput_container ginput_container_email">
                                            <input name="email" id="email" type="text" value="" class="large" placeholder="Ex. name@company.com">
                                        </div>
                                    </li>
                                    <li id="field_29_5" class="gfield field_sublabel_below field_description_below">
                                        <label class="gfield_label" for="phone">Phone</label>
                                        <div class="ginput_container ginput_container_text">
                                            <input name="phone" id="phone" type="text" value="" class="large" tabindex="4">
                                        </div>
                                    </li>
                                    <li id="field_29_3" class="gfield gfield_contains_required field_sublabel_below field_description_below">
                                        <label class="gfield_label" for="message">Copy &amp; Paste Resume/CV with Positions of Interest Include Portfolio or Other Credential (i.e. Linkedin) Links if Applicable<span class="gfield_required">*</span></label>
                                        <div class="ginput_container ginput_container_textarea">
                                            <textarea name="message" id="message" class="textarea large" tabindex="5" placeholder="Enter a message" rows="10" cols="50"></textarea>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                            <div class="modal-btn-submit"> 
                                <button type="button" class="btn btn-lg btn-pink" onclick="submitCareer(event)" data-loading-text="Contact <i class='fa fa-circle-o-notch fa-spin'></i> ">
                                    Contact
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modal__affiliate" class="modal " style="display: none;">
        <div class="modal__content">
            <div class="modal__close"><i class="icon-close">X</i></div>
            <div id="" class="modal_body"><h2 style="text-align: left;">Affiliate&nbsp;Inquiries</h2>
                <div id="submitsuccess_affiliate" class="alert alert-success" style="display:none;">

                </div>
                <div id="submiterror_affiliate" class="alert alert-danger" style="display:none;">

                </div>
                <p>We are always looking to meet like-minded, passionate partners. Please fill out the form below and our team will get in touch with you promptly.</p>
                <div class="form-holder">
                    <div class="gf_browser_gecko gform_wrapper" id="gform_wrapper_32">
                        <a id="gf_32" class="gform_anchor"></a>
                        <form  id="affiliateForm" >
                            <input type="hidden" name="action" value="affiliate">

                            <div class="gform_body">
                                <ul id="" class="gform_fields top_label form_sublabel_below description_below">
                                    <li id="" class="gfield medium-6 columns gfield_contains_required field_sublabel_below field_description_below">
                                        <label class="gfield_label" for="first_name">First Name<span class="gfield_required">*</span></label>
                                        <div class="ginput_container ginput_container_text">
                                            <input name="first_name" id="first_name" type="text" value="" class="medium" tabindex="1" placeholder="Enter First Name">
                                        </div>
                                    </li>
                                    <li id="field_29_7" class="gfield medium-6 columns gfield_contains_required field_sublabel_below field_description_below">
                                        <label class="gfield_label" for="last_name">Last Name<span class="gfield_required">*</span></label>
                                        <div class="ginput_container ginput_container_text">
                                            <input name="last_name" id="last_name" type="text" value="" class="medium" tabindex="2" placeholder="Enter Last Name">
                                        </div>
                                    </li>
                                    <li id="field_29_1" class="gfield gfield_contains_required field_sublabel_below field_description_below">
                                        <label class="gfield_label" for="email">Email<span class="gfield_required">*</span></label>
                                        <div class="ginput_container ginput_container_email">
                                            <input name="email" id="email" type="text" value="" class="large" placeholder="Ex. name@company.com">
                                        </div>
                                    </li>
                                    <li id="field_29_5" class="gfield field_sublabel_below field_description_below">
                                        <label class="gfield_label" for="phone">Phone</label>
                                        <div class="ginput_container ginput_container_text">
                                            <input name="phone" id="phone" type="text" value="" class="large" tabindex="4">
                                        </div>
                                    </li>
                                    <li id="field_29_3" class="gfield gfield_contains_required field_sublabel_below field_description_below">
                                        <label class="gfield_label" for="message">Please tell us briefly about your company and the opportunity you wish to discuss. Please include website and/or Linkedin links if available<span class="gfield_required">*</span></label>
                                        <div class="ginput_container ginput_container_textarea">
                                            <textarea name="message" id="message" class="textarea large" tabindex="5" placeholder="Enter a message" rows="10" cols="50"></textarea>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                            <div class="modal-btn-submit"> 
                                <button type="button" class="btn btn-lg btn-pink" onclick="submitAffiliate(event)" data-loading-text="Contact <i class='fa fa-circle-o-notch fa-spin'></i> ">
                                    Contact
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal_contact_success" class="modal success_modal" style="display: none;">
        <div class="modal__content">
            <div class="modal__close"><i class="icon-close">X</i></div>
            <div id="" class="modal_body"><h2 style="text-align: left;">Contact Us</h2>
                <p>&nbsp;</p>
<!--                <p><b>Toll Free Number:</b> (888)-737-8902</p>
                <p><b>Outside Canada or USA:</b> (647)-360-7477</p>-->
                <?php include_once 'contactnumberpartial.php';?>
                <p style="text-align: left;">We are here to answer any question you may have about our products. Please fill out the contact form and we will respond as soon as we can.</p>
                <div class="form-holder">
                    <div id="" class="gforms_confirmation_message">
                        <meta charset="UTF-8">
                        <div id="" class="gform_confirmation_wrapper ">
                            <div id="" class="gform_confirmation_message">
                                Thanks for contacting us! We will get in touch with you shortly.
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modal_careers_success" class="modal success_modal" style="display: none;">
        <div class="modal__content">
            <div class="modal__close"><i class="icon-close">X</i></div>
            <div id="" class="modal_body"><h2 style="text-align: left;">Career Inquiries</h2>
                <p>We are always looking to meet talented, passionate team players who are natural thinkers and doers. Please fill out the form below if you are looking for an exciting new role in <strong>technology, design, sales or marketing.</strong><br>
                    Our HR team will get in touch with you with potential opportunities.</p>
                <div class="form-holder">
                    <div id="gforms_confirmation_message_29" class="gform_confirmation_message_29 gforms_confirmation_message">
                        <div id="gform_confirmation_wrapper_29" class="gform_confirmation_wrapper ">
                            <div id="gform_confirmation_message_29" class="gform_confirmation_message_29 gform_confirmation_message">
                                Thanks for contacting us! We will get in touch with you shortly.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modal_affiliate_success" class="modal success_modal" style="display: none;">
        <div class="modal__content">
            <div class="modal__close"><i class="icon-close">X</i></div>
            <div id="" class="modal_body"><h2 style="text-align: left;">Affiliate Inquiries</h2>
                <p>We are always looking to meet like-minded, passionate partners. Please fill out the form below and our team will get in touch with you promptly.</p>
                <div class="form-holder">
                    <div id="gforms_confirmation_message_29" class="gform_confirmation_message_29 gforms_confirmation_message">
                        <div id="gform_confirmation_wrapper_29" class="gform_confirmation_wrapper ">
                            <div id="gform_confirmation_message_29" class="gform_confirmation_message_29 gform_confirmation_message">
                                Thanks for contacting us! We will get in touch with you shortly.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div id="demo_site_msg" class="modal success_modal" style="display: none;">
        <div class="modal__content">
            <div class="modal__close"><i class="icon-close">X</i></div>
            <div id=""><h2 style="text-align: left;">Coming Soon</h2>
                <p>We are currently in final beta testing and will be going live shortly.  If you would like to be part of our beta testing customers on the platform, please contact us.</p>
            </div>
        </div>
    </div>
<!--    -->

</div>

<script>
    $(document).ready(function () {
        $('input, textarea').click(function () {
            //  $(this).removeClass('error').val('');
            $(this).removeClass('error');
        });
        $('input, textarea').blur(function () {
            //  $(this).removeClass('error').val('');
            $(this).removeClass('error');
        });
        $('input, textarea').focus(function () {
            //  $(this).removeClass('error').val('');
            $(this).removeClass('error');
        });
        $(".modal__close").click(function () {
            $(this).parent().parent().modal("hide");
        });
    });

    //submitAffiliate
    function submitAffiliate($event) {
        var element = $($event.currentTarget);


        var fields = ["message", "email", "last_name", "first_name"]// 

        var i, l = fields.length;
        var fieldname;
        var noError = true;

        for (i = 0; i < l; i++) {
            fieldname = fields[i];
            if (document.forms["affiliateForm"][fieldname].value === "") {

                $('#affiliateForm #' + fieldname).addClass('error');
                console.log(fieldname, $('#' + fieldname).length);

                //  alert(fieldname + " can not be empty");
                noError = false;
            }
            if (fieldname === "email" && document.forms["affiliateForm"][fieldname].value != "") {
                var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                if (document.forms["affiliateForm"][fieldname].value.match(mailformat))
                {
                    //  alert('ok');

                } else {
                    noError = false;
                    $('#affiliateForm #' + fieldname).addClass('error');
                    //  alert('notok');
                }

            }

        }

        if (!noError) {
            return false;
        }
        element.button('loading');
        $.ajax({
            type: "POST",
            url: "/ajaxprocess.php",
            data: $("#affiliateForm").serialize(),
            dataType: "JSON",
            success: function (data) {
                console.log('success');
                console.log(data);
                $("#submitsuccess").hide();
                $("#submiterror").hide();
                if (data.msg == '') {
//                    $("#submitsuccess_affiliate").html("Affiliate Form is submitted successfully. Our representative will contact you shortly");
//                    $("#submitsuccess_affiliate").show();
                    $("#modal__affiliate").modal("hide");
                    $("#modal_affiliate_success").modal("show");
                    $("#contactUsForm")[0].reset()
                } else {
                    $("#submiterror_affiliate").html(data.msg);
                    $("#submiterror_affiliate").show();
                }
                element.button('reset');
                // 
            },
            error: function (data) {
                console.log('error');
                console.log(data);
                $("#submiterror_affiliate").html(data.msg);
                $("#submiterror_affiliate").show();
                element.button('reset');
            }
        });
        return false;
    }

    function submitContactUs($event) {
        var element = $($event.currentTarget);


        var fields = ["message", "email", "last_name", "first_name"]// 

        var i, l = fields.length;
        var fieldname;
        var noError = true;

        for (i = 0; i < l; i++) {
            fieldname = fields[i];
            if (document.forms["contactUsForm"][fieldname].value === "") {

                $('#contactUsForm #' + fieldname).addClass('error');
                console.log(fieldname, $('#' + fieldname).length);

                //  alert(fieldname + " can not be empty");
                noError = false;
            }
            if (fieldname === "email" && document.forms["contactUsForm"][fieldname].value != "") {
                var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                if (document.forms["contactUsForm"][fieldname].value.match(mailformat))
                {
                    //  alert('ok');

                } else {
                    noError = false;
                    $('#contactUsForm #' + fieldname).addClass('error');
                    //  alert('notok');
                }

            }

        }

        if (!noError) {
            return false;
        }
        element.button('loading');
        $.ajax({
            type: "POST",
            url: "/ajaxprocess.php",
            data: $("#contactUsForm").serialize(),
            dataType: "JSON",
            success: function (data) {
                console.log('success');
                console.log(data);
                $("#submitsuccess").hide();
                $("#submiterror").hide();
                if (data.msg == '') {
//                    $("#submitsuccess_contact").html("Contact us is submitted successfully. Our representative will contact you shortly");
//                    $("#submitsuccess_contact").show();

                    $("#modal__contact").modal("hide");
                    $("#modal_contact_success").modal("show");
                    $("#contactUsForm")[0].reset();
                } else {
                    $("#submiterror_contact").html(data.msg);
                    $("#submiterror_contact").show();
                }
                element.button('reset');
                // 
            },
            error: function (data) {
                console.log('error');
                console.log(data);
                $("#submiterror_contact").html(data.msg);
                $("#submiterror_contact").show();
                element.button('reset');
            }
        });
        return false;
    }



    function submitCareer($event) {
        var element = $($event.currentTarget);


        var fields = ["message", "email", "last_name", "first_name"]// 

        var i, l = fields.length;
        var fieldname;
        var noError = true;

        for (i = 0; i < l; i++) {
            fieldname = fields[i];
            if (document.forms["careerForm"][fieldname].value === "") {

                $('#careerForm #' + fieldname).addClass('error');
                console.log(fieldname, $('#' + fieldname).length);

                //  alert(fieldname + " can not be empty");
                noError = false;
            }
            if (fieldname === "email" && document.forms["careerForm"][fieldname].value != "") {
                var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                if (document.forms["careerForm"][fieldname].value.match(mailformat))
                {
                    //  alert('ok');

                } else {
                    noError = false;
                    $('#careerForm #' + fieldname).addClass('error');
                    //  alert('notok');
                }

            }

        }

        if (!noError) {
            return false;
        }
        element.button('loading');
        $.ajax({
            type: "POST",
            url: "/ajaxprocess.php",
            data: $("#careerForm").serialize(),
            dataType: "JSON",
            success: function (data) {
                console.log('success');
                console.log(data);
                $("#submitsuccess").hide();
                $("#submiterror").hide();
                if (data.msg == '') {
//                    $("#submitsuccess_career").html("Career information is submitted successfully. Our representative will contact you shortly");
//                    $("#submitsuccess_career").show();
                    $("#modal__careers").modal("hide");
                    $("#modal_careers_success").modal("show");
                    $("#contactUsForm")[0].reset()
                } else {
                    $("#submiterror_career").html(data.msg);
                    $("#submiterror_career").show();
                }
                element.button('reset');
                // 
            },
            error: function (data) {
                console.log('error');
                console.log(data);
                $("#submiterror_career").html(data.msg);
                $("#submiterror_career").show();
                element.button('reset');
            }
        });
        return false;
    }
</script>
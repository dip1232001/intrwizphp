<?php include_once 'partials/head.php'; ?>
<?php include_once 'partials/navbar.php'; ?>

<style>
    .contactUsMsgForm textarea.error, .contactUsMsgForm input.error {
        border: 1px solid red !important;
    }
</style>
<!-- Body Section Start -->
<div id="inner-body-wrap">
    <div class="container">
        <div class="contact-wrap">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-8">
                    <h2>Contact Us</h2>
                    <p>We would love to here from you. Please use the form below to send us a comment or ask a question. We will reply as soon as possible.</p>
                    <p>You can also give us a call at:</p>
                    <div class="contact-list">
                        <div class="row">
                            <div class="col-xs-5 col-md-2 col-lg-2"><span>USA</span></div>
                            <div class="col-xs-7 col-md-4 col-lg-4"><a href="#">+1 -(800)- 799-4451</a></div>
                            <div class="col-xs-5 col-md-2 col-lg-2"><span>Pakistan</span></div>
                            <div class="col-xs-7 col-md-4 col-lg-4"><a href="#">1 -123-123-1234</a></div>
                            <div class="col-xs-5 col-md-2 col-lg-2"><span>Canada</span></div>
                            <div class="col-xs-7 col-md-4 col-lg-4"><a href="#">+1 -(647)- 799-0250</a></div>
                            <div class="col-xs-5 col-md-2 col-lg-2"><span>Kenya</span></div>
                            <div class="col-xs-7 col-md-4 col-lg-4"><a href="#">1 -123-123-1234</a></div>
                            <div class="col-xs-5 col-md-2 col-lg-2"><span>Mexico</span></div>
                            <div class="col-xs-7 col-md-4 col-lg-4"><a href="#">+52 -(55)- 85261991</a></div>
                            <div class="col-xs-5 col-md-2 col-lg-2"><span>Tanzani</span></div>
                            <div class="col-xs-7 col-md-4 col-lg-4"><a href="#">1 -123-123-1234</a></div>
                            <div class="col-xs-5 col-md-2 col-lg-2"><span>London</span></div>
                            <div class="col-xs-7 col-md-4 col-lg-4"><a href="#">+44 -(20)- 83966666</a></div>
                            <div class="col-xs-5 col-md-2 col-lg-2"><span>Uganda</span></div>
                            <div class="col-xs-7 col-md-4 col-lg-4"><a href="#">1 -123-123-1234</a></div>
                            <div class="col-xs-5 col-md-2 col-lg-2"><span>Kuwait</span></div>
                            <div class="col-xs-7 col-md-4 col-lg-4"><a href="#">1 -123- 123-1234</a></div>
                            <div class="col-xs-5 col-md-2 col-lg-2"><span>Australia</span></div>
                            <div class="col-xs-7 col-md-4 col-lg-4"><a href="#">1 -123-123-1234</a></div>
                            <div class="col-xs-5 col-md-2 col-lg-2"><span>Sweden</span></div>
                            <div class="col-xs-7 col-md-4 col-lg-4"><a href="#">1 -123-123-1234</a></div>
                            <div class="col-xs-5 col-md-2 col-lg-2"><span>New Zealand</span></div>
                            <div class="col-xs-7 col-md-4 col-lg-4"><a href="#">1 -123-123-1234</a></div>
                            <div class="col-xs-5 col-md-2 col-lg-2"><span>UAE</span></div>
                            <div class="col-xs-7 col-md-4 col-lg-4"><a href="#">1 -123-123-1234</a></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <div class="gray-box">
                        <div class="row">
                            <form class="contactUsMsgForm" id="contactUsMsgForm">
                                <input type="hidden" name="action" value="contactUs" />
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div id="submitsuccess_contactMsg" class="alert alert-success" style="display:none;">

                                    </div>
                                    <div id="submiterror_contactMsg" class="alert alert-danger" style="display:none;">

                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-6 col-lg-6">
                                    <label>First Name</label>
                                    <input type="text" name="first_name" id="first_name">
                                </div>
                                <div class="col-xs-6 col-md-6 col-lg-6">
                                    <label>Last Name</label>
                                    <input type="text" name="last_name" id="last_name">
                                </div>
                                <div class="col-xs-12 col-md-12 col-lg-12">
                                    <label>Email Address</label>
                                    <input type="email" name="email" id="email">
                                </div>
                                <div class="col-xs-12 col-md-12 col-lg-12">
                                    <label>Message</label>
                                    <textarea name="message" id="message"></textarea>
                                </div>
                                <div class="col-xs-12 col-md-12 col-lg-12">
                                    <button type="button" class="btn btn-lg btn-pink" onclick="sendContacUsMsg(event)" data-loading-text="Send Message <i class='fa fa-circle-o-notch fa-spin'></i> ">
                                        Send Message
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body Section End -->

<?php include_once 'partials/footer.php'; ?>

<script>

    function sendContacUsMsg($event) {
        var element = $($event.currentTarget);


        var fields = ["message", "email", "last_name", "first_name"]// 

        var i, l = fields.length;
        var fieldname;
        var noError = true;

        for (i = 0; i < l; i++) {
            fieldname = fields[i];
            if (document.forms["contactUsMsgForm"][fieldname].value === "") {

                $('#contactUsMsgForm #' + fieldname).addClass('error');
                console.log(fieldname, $('#' + fieldname).length);

                //  alert(fieldname + " can not be empty");
                noError = false;
            }
            if (fieldname === "email" && document.forms["contactUsMsgForm"][fieldname].value != "") {
                var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                if (document.forms["contactUsMsgForm"][fieldname].value.match(mailformat))
                {
                    //  alert('ok');

                } else {
                    noError = false;
                    $('#contactUsMsgForm #' + fieldname).addClass('error');
                    //  alert('notok');
                }

            }

        }

        if (!noError) {
            return false;
        }
        element.button('loading');
        $.ajax({
            type: "POST",
            url: "/ajaxprocess.php",
            data: $("#contactUsMsgForm").serialize(),
            dataType: "JSON",
            success: function (data) {
                console.log('success');
                console.log(data);
                $("#submitsuccess_contactMsg").hide();
                $("#submiterror_contactMsg").hide();
                if (data.msg == '') {
                    
                    $("#modal_contact_success").modal("show");
                    $("#contactUsMsgForm")[0].reset();
                } else {
                    $("#submiterror_contactMsg").html(data.msg);
                    $("#submiterror_contactMsg").show();
                }
                element.button('reset');
                // 
            },
            error: function (data) {
                console.log('error');
                console.log(data);
                $("#submiterror_contactMsg").html("There was some error, please try again");
                $("#submiterror_contactMsg").show();
                element.button('reset');
            }
        });
        return false;
    }
</script>

</body>
</html>

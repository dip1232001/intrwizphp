<?php include_once 'partials/head.php'; ?>
<?php include_once 'partials/navbar.php'; ?>


<!-- Body Section Start -->
<div id="inner-body-wrap">
    <div class="pricing-section">
        <div class="container">
            <!--            <h2>Our Pricing</h2>
                        <h4>Our pricing is flexible so you can get-and pay for-exactly what you need. No secret decoder ring required.</h4>-->
            <div class="pricing-option-panel">
                <div class="row dsp-flex">
                    <div class="col-lg-6 col-md-6 col-sm-12 bg-gray">
                        <div class="pricing-block">
                            <div class="pricing-top-panel">
                                <h2>Turn Key Branded Solution</h2>
                                <h3>Free</h3>
                            </div>
                            <div class="pricing-content">
                                <ul class="check-bullet">
                                    <li>Collaborative Client Facing Portal creating instant Branded Interactive Micro Sites on each search</li>
                                    <li>Interactive Agent Dashboard to provide you with unparalleled business intelligence on each customer with ability to interact in real time with each Branded Interactive Micro Site</li>

                                    <li>Commissions paid to you using our direct net negotiated rates <span><a class="text-pink" href="#commision-section"> See Commission Grid </a></span></li>

                                    <li>Each Interactive Micro Site loaded with relevant Destination Content powered by ArrivalGuides</li>

                                    <li>Intrwiz has hundreds of Suppliers worldwide to choose from, ready to trade in any market around the world with 58 currencies supported.</li>
                                    <li>We manage all payments, confirm reservations and provide 24 hour support to you and your travellers.</li>
                                    <li>We are Integrated with all three GDS's and access to ovr 900 global Airlines for private fares.</li>
                                    <li>Big Data Reporting</li>
                                    <li>Build in seconds and ready to trade your domain instantly.</li>
                                    <li>Fully Customizable</li>
                                </ul>
                            </div>
                            <div class="pricing-footer">
                                <a <?php echo (IS_PROD) ? "onclick='$(\"#demo_site_msg\").modal(\"show\")' href='javascript:void(0);'" : 'href="/register.php"'; ?>  class="red-button">Sign Up</a>
                                <p>Order now at no Cost</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 bg-gray">
                        <div class="pricing-block">
                            <div class="pricing-top-panel">
                                <h2>Custom Setup Using Your Content</h2>
                                <h3>$ 1,000</h3>
                                <p>One Time setup cost per agency plus<br/><strong>$25 per agent/month</strong></p>
                            </div>
                            <div class="pricing-content">
                                <ul class="check-bullet">
                                    <li>Collaborative Client Facing Portal creating instant Branded Interactive Micro Sites on each search</li>
                                    <li>Interactive Agent Dashboard to provide you with unparalleled business intelligence on each customer with ability to interact in real time with each Branded Interactive Micro Site</li>



                                    <li>Each Interactive Micro Site loaded with relevant Destination Content powered by ArrivalGuides</li>



                                    <li>We are Integrated with all three GDS's and access to ovr 900 global Airlines for private fares.</li>
                                    <li>Big Data Reporting</li>

                                    <li>Fully Customizable</li>



                                </ul>
                                <h4>Content Connectivity Fees:</h4>
                                <ul class="disc-bullet">
                                    <li><strong>GDS Connectivity:</strong> $1,500 + $5.00 Per ticket</li>
                                    <li><strong>Hotel Suppliers:</strong> $500 per supplier + 1% gross selling price</li>
                                    <li><strong>Car, Excursions, Tours, Cruise:</strong> $500 per supplier (plus any api connectivity fee where applicable).</li>
                                </ul>
                            </div>
                            <div class="pricing-footer">
                                <a href="javascript:void(0);" onclick="$('#modal__contact').modal('show');" class="red-button">Contact Us</a>
                                <p>CONTACT US TO SET UP YOUR CUSTOMIZED PORTAL WITH YOUR CONTENT. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Commisions Section Start -->
    <div class="section-Commisions text-center" id="commision-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <h2 class="text-center">Intrwiz Commissions</h2>
                    <p>We Manage the full booking process including 24/7 customer service.   You simply receive your commission check bi-weekly on all confirmed sales (commissions are calculated on the gross selling price before taxes) </p>
                    <div class="commisions-list-wrap">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="sub-wh-box">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2 col-xs-3"><img src="img/home/svg/icon-flight.svg"></div>
                                        <div class="col-lg-5 col-md-5 col-xs-3 no-pad">
                                            <h2>Flights</h2>
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-xs-6">
                                            <div class="pink-percent-txt" style="text-transform: none;">Up to 25%</div>
                                            <div class="pink-percent-txt hide">$5.00<span>per private air booking</span></div>
                                            <div class="pink-percent-txt hide">TBD</div>
                                        </div>
                                    </div>
                                    <ul>
                                        <li>Direct Negotiated fares with over 750 airlines </li>
<!--                                        <li>70+ point of sale countries</li>
                                        <li>190+ Countries Connected</li>-->
                                        <li>Book on your GDS and Que your bookings to your agent dashboard</li>
                                        <li class="hide">Add your additional markups of up to 50% on Intrwiz private negotiated fares and still be competitive</li>
                                        <li>Access our direct net rates and add your markups to earn as much as 25% commissions and still be competitive. If you have a floating deposit in place with us, your pricing will reduce by an additional 3% for credit card fees saved.</li>
                                        <li>24/7 Call Centre Support  </li>
                                    
                                    </ul>
                                </div>
                                <div class="sub-wh-box">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2 col-xs-3"><img src="img/home/svg/icon-hotel.svg"></div>
                                        <div class="col-lg-5 col-md-5 col-xs-3 no-pad">
                                            <h2>Hotels</h2>
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-xs-6">
                                            <div class="pink-percent-txt" style="text-transform: none;">Up to 25%</div>
                                        </div>
                                    </div>
                                    <ul>
                                        <li>Direct Negotiated rates for over 1 million hotels</li>
                                        <li>Coverage over 240 countries</li>
                                        <li>Access our direct net rates and add your markups to earn as much as 25% commissions and still be competitive. If you have a floating deposit in place with us, your pricing will reduce by an additional 3% for credit card fees saved.</li>
                                        <li>24/7 Call Centre Support  </li>
                                    </ul>
                                </div>
                                <div class="sub-wh-box">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2 col-xs-3"><img src="img/home/svg/icon-car.svg"></div>
                                        <div class="col-lg-5 col-md-5 col-xs-5 no-pad">
                                            <h2>Cars</h2>
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-xs-4">
                                            <div class="pink-percent-txt">8%</div>
                                        </div>
                                    </div>
                                    <ul>
                                       
                                        <li>Direct negotiated rates with all major car suppliers  </li>
                                        <li>Coverage to over 250 countries </li>
                                        <li>24/7 Call Centre Support  </li>
                                    </ul>
                                </div>
                                <div class="sub-wh-box">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2 col-xs-3"><img src="img/home/svg/icon-tours.svg"></div>
                                        <div class="col-lg-5 col-md-5 col-xs-5 no-pad">
                                            <h2>Tours</h2>
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-xs-4">
                                            <div class="pink-percent-txt">12%</div>
                                        </div>
                                    </div>
                                    <ul>
                                        <li>Multi Day tours for every type of experience powered by world class Tour Operators from around the world</li>
                                        <li>Coverage to over 200 countries</li>
                                        <li>24/7 Call Centre Support  </li>
                                    </ul>
                                </div>
                                <div class="sub-wh-box">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2 col-xs-3"><img src="img/home/svg/icon-cruise.svg"></div>
                                        <div class="col-lg-5 col-md-5 col-xs-5 no-pad">
                                            <h2>Cruises</h2>
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-xs-4">
                                            <div class="pink-percent-txt">12%</div>
                                        </div>
                                    </div>
                                    <ul>
                                        
                                        <li>Directly connected to all major cruise lines.</li>
                                        <li>Global Fulfilment  </li>
                                        <li>24/7 Call Centre Support  </li>
                                    </ul>
                                </div>
                                <div class="sub-wh-box">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-2 col-xs-3"><img src="img/home/svg/icon-camera.png"></div>
                                        <div class="col-lg-5 col-md-5 col-xs-5 no-pad">
                                            <h2>Activities</h2>
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-xs-4">
                                            <div class="pink-percent-txt">8%</div>
                                        </div>
                                    </div>
                                    <ul>
                                        <li>Handpicked world class activities from around the world</li>
                                        <li>Coverage to over 300 Countries.  </li>
                                        <li>24/7 Call Centre Support  </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Commisions Section End -->
</div>
<!-- Body Section End -->



<?php include_once 'partials/footer.php'; ?>

</body>
</html>

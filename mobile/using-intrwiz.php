<?php include_once 'partials/head.php'; ?>
<?php include_once 'partials/navbar.php'; ?>



<div class="inner-page-banner-get-started">
        <div class="header-page-title">
            <h2>Using <span>Intrwiz</span></h2>
            <h3>Welcome to the Intrwiz Platform!</h3>
        </div>
    </div>

    <!-- Body Section Start -->
    <div id="getting-started-wrap">
        <div class="container">
            <div class="getting-started-section">
                <h2>Welcome to <span>Intrwiz<span></h2>
                <p>Your real time collaborative platform built specifically for travel agents to capture and convert your leads in bookings in a manner never done before. In this guide we will walk you through how to get set up and running. We will also guide you through how to customize and use this platform step by step.</p>

                <p>If you are new to Intrwiz, please allow 30 - 45 minutes to complete the whole guide while you follow along in the app. Feel free to also join us for an upcoming live interactive webinar. We would love to see you there.</p>
                <p>To help you get started, this guide will walk through:</p>
                <ul>
                    <li><a href="#">What is lntrwiz</a></li>
                    <li><a href="#">Using lntrwiz</a></li>
                    <li><a href="#">Configuring and personalizing the platform</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Body Section End -->



<?php include_once 'partials/footer.php'; ?>

</body>
</html>

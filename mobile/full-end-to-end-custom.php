<?php include_once 'partials/head.php'; ?>
<?php include_once 'partials/navbar.php'; ?>


<!-- Body Section Start -->
<div id="inner-body-wrap">
    <div class="container">
        <div class="collaborate-solution">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="left-content-panel">
                        <h2>Full End to End Custom Online Booking Platforms</h2>
                        <ul>
                            <li>Customized Customer Facing Solution.</li> 
                            <li>Control panel to set up or adjust markups.</li>
                            <li>Dashboard to manage bookings.</li> 
                            <li>24/7 call centre support (optional)</li>
                        </ul>
                        <div class="pricing-text-section">
                            <h4>Pricing based on customization required. To set up your Custom Site , please contact us.</h4>
                        </div>
                        <p class="text-center"><a href="javascript:void(0)" onclick="$('#modal__contact').modal('show');" class="red-button">Contact Us</a></p>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="sample-clients-panel">
                        <h3>Sample Clients</h3>
                        <!-- sample row -->
                        <div class="row mb20">
                            <div class="col-md-12 col-sm-12">
                                <div class="sample-logo-panel">
                                    <img src="img/arrival-guides-logo.jpg" class="img-responsive">
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <img src="img/thumbnail-AG.jpg" class="thumbnail-img img-responsive">
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <div class="sample-logo-panel">
                                    <p><a href="<?php echo AG_LINK?>" target="_blank">View Live Site</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="row mb20">
                            <div class="col-md-12 col-sm-12">
                                <div class="sample-logo-panel">
                                    <img src="img/vio-travel-logo.jpg" class="img-responsive">
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <img src="img/thumbnail-vio.jpg" class="thumbnail-img img-responsive">
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <div class="sample-logo-panel">
                                    <p><a href="<?php echo VIO_LINK?>" target="_blank">View Live Site</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Body Section End -->



<?php include_once 'partials/footer.php'; ?>

</body>
</html>

<?php include_once 'partials/head.php'; ?>
<?php include_once 'partials/navbar.php'; ?>


<!-- Body Section Start -->
<div id="inner-body-wrap">
   <div class="pricing-section">
<div class="container">
<!--            <h2>Our Pricing</h2>
<h4>Our pricing is flexible so you can get-and pay for-exactly what you need. No secret decoder ring required.</h4>-->
<div class="pricing-option-panel">
<div class="row dsp-flex">

</div>
<div class="col-lg-12 col-md-12 col-sm-12">
<div class="pricing-block">
<div class="left-content-panel ">
<h2 class="price_heading">
Intrwiz Travel Agent Products, Pricing &amp; Content
</h2>                        
</div>


<div class="pricing-content">
<h2><i class="fa fa-check"></i> Branded Customer Management Tools</h2>
<ul class="check-bullet">

<li>Intrwiz collaborative customer management tools Integrated with The Intrwiz Travel
Agent Dashboard provides you with unparalleled business intelligence on each
customer to seamlessly convert your leads into bookings.</li>
<li>There is no cost to use Intrwiz’s core Customer Management Tools.</li>

</ul>

<h2><i class="fa fa-check"></i>  Branded Itinerary Solution</h2>
<ul class="check-bullet">

<li>Que your GDS to automatically create Collaborative Itineraries on each air booking
to manage your customers through their trip pre-loaded with rich destination content
powered by ArrivalGuides.</li>
<li>Enable ancillary bookable content to automatically maximize your earnings potential
on each booking.</li>
<li>There is no cost to use the Intrwiz Itinerary platform. For any ancillary bookable
content you enable on the itinerary (including Air, Hotel, Car and Activities), Intrwiz
will charge a transaction fee as articulated below.</li>

</ul>



<h2><i class="fa fa-check"></i> Air Online Booking Solution</h2>
<ul class="check-bullet">

<li>Full turn-key Branded B2B and B2B2C Collaborative Air Booking Solution (with the
ability to set B2C markups).</li>
<li>Connect to your GDS or access Intrwiz Global LCC, Published and private fares.</li>
<!-- <li>There is no cost to use the Intrwiz Itinerary platform. For any ancillary bookable
content you enable on the itinerary (including Air, Hotel, Car and Activities), Intrwiz
will charge a transaction fee as articulated below.</li> -->

<li>Each booking seamlessly integrated into the Intrwiz Travel Agent Dashboard to
provide full visibility on each booking.</li>

<li>Intrwiz charges a fixed transaction fee of $5.00 USD per air booking processed
through the system as well as 3% for online credit card processing. There is also a
one-time fee of $1,500 USD to set up your direct GDS Credentials.</li>

<li>We manage all payments, confirm reservations and provide 24hour support to you
and your travelers when using Intrwiz Content.</li>

<li>Ready to trade in any market around the world with 58 currencies supported.</li>
<li>Real-time Reporting with Back Office Integration via XML.</li>

</ul>


<h2><i class="fa fa-check"></i> Hotel Online Booking Solution</h2>
<ul class="check-bullet">

<li>Full turn-key Branded B2B and B2B2C Collaborative Booking Solution (with the
ability to set B2C markups).</li>
<li>Directly connected all major hotel content providers.</li>
<li>Each B2B or B2B2C booking seamlessly integrated into the Intrwiz Travel Agent
Dashboard to provide full visibility on each booking.</li>
<li>Leverage Intrwiz Content or use your own credentials and suppliers. There is a one-
time fee of $500 USD per Supplier to set up your own Credentials.</li>
<li>Intrwiz charges a transaction fee of 2% of the gross selling as well as a 3% credit
card processing fee (where applicable).</li>
<li>We manage all payments, confirm reservations and provide 24hour support to you
and your travelers when using Intrwiz Content.</li>
<li>Ready to trade in any market around the world with 58 currencies supported.</li>
<li>Real-time Reporting with Back Office Integration via XML</li>


</ul>




<h2><i class="fa fa-check"></i> Activities Online Booking Solution</h2>
<ul class="check-bullet">

<li>Fully Branded B2B and B2B2C Collaborative Booking Solution Connected to Viator.</li>
<li>Use your own credentials or leverage Intrwiz gross rates.</li>
<li>Each B2B and B2B2C booking seamlessly integrated into the Intrwiz Travel Agent
Dashboard to provide full visibility on each booking.</li>
<li>Intrwiz charges a 2% transaction fee on any activities purchased through the platform
- You earn the rest.</li>
<li>Real-time Reporting with Back Office Integration via XML</li>



</ul>



<h2><i class="fa fa-check"></i> Car Online Booking Solution</h2>
<ul class="check-bullet">

<li>Fully Branded B2B and B2B2C Booking Solution Connected to CarTrawler.</li>
<li>Use your own credentials or leverage Intrwiz gross rates.</li>
<li>Each B2B or B2B2C booking seamlessly integrated into the Intrwiz Travel Agent
Dashboard to provide full visibility on each booking.</li>
<li>Intrwiz charges a 2% transaction fee on any car rentals purchased through the
platform.</li>
<li>Real-time Reporting with Back Office Integration via XML</li>



</ul>


<h2><i class="fa fa-check"></i> Tours and Cruises Lead Gen and Customer Management Tools</h2>
<ul class="check-bullet">

<li>Use Intrwiz Customer Management Tools combined with Intrwiz Travel Agent
Dashboard integrated with Intrwiz Tour and Cruise content to capture, convert and
manage your customers.</li>
<li>Add links to your site and social channels to generate new leads.</li>
<li>There is no cost to use this functionality.</li>


</ul>

</div>
<div class="col-md-6">
<div class="pricing-footer">
<a <?php echo (IS_PROD) ? "onclick='$(\"#demo_site_msg\").modal(\"show\")' href='javascript:void(0);'" : 'href="/register.php"'; ?>  class="red-button">Sign Up</a>
<p><strong>Order now at no Cost</strong></p>
</div>
</div>
<div class="col-md-6">
<div class="pricing-footer">
<a href="javascript:void(0)" onclick="$('#modal__contact').modal('show');" class="red-button">Contact Us</a>
<p><strong>CONTACT US TO SET UP YOUR CUSTOMIZED PORTAL WITH YOUR CONTENT</strong> </p>
</div>
</div>

</div>
</div>
</div>
</div>
</div>

</div>
<!-- Body Section End -->



<?php include_once 'partials/footer.php'; ?>

</body>
</html>

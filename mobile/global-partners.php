<?php include_once 'partials/head.php'; ?>
<?php include_once 'partials/navbar.php'; ?>


<!-- Body Section Start -->
<div id="global-partners-body-wrap">
    <div class="global-partners">
        <div class="left-content-panel">
            <h2>Global Distribution Partners</h2> 
            <div><img src="/img/global-partner-img.jpg" class="img-responsive"></div>
            <div class="container">                                                           
                <p>Intrwiz is appointing global distribution partners to deploy the Intrwiz suite of products as part of our overall global strategy. If you are interested in a particular region, please Contact Us.</p>
                <p class="text-center"><a href="javascript:void(0)" onclick="$('#modal__contact').modal('show');" class="red-button">Contact Us</a></p>
            </div>
        </div>
    </div>
</div>
<!-- Body Section End -->



<?php include_once 'partials/footer.php'; ?>

</body>
</html>

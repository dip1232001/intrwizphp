<!-- Navigation Start -->    
<nav class="navbar navbar-inverse" role="navigation">        
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img src="img/logo.png" alt="" class="img-responsive"></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown open">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Products <span class="caret"></span></a>
                    <ul class="dropdown-menu-open">
                        <li><a href="agent-platform.php">Agent Platform</a></li>
                        <li><a href="affinity-site.php">Closed User Group Afinity Sites</a></li>
                        <li><a href="full-end-to-end-custom.php">Full End to End Custom Online Booking Platform</a></li>
                        <li><a href="collaborative-sollution.php">Collaborative Itinerary Solutions</a></li>                       

                    </ul>
                </li>
                
                <?php 
                    if(!IS_PROD):
                ?>
                
                <li class="hide">
                    <a href="using-intrwiz.php">Using Intrwiz</a>
                </li>
                
                <?php 
                    endif;
                ?>
                <?php /* ?>
                <li>
                    <a href="pricing.php">Pricing</a>
                </li>
                <?php */ ?>
                <li>
                    <a href="global-partners.php">Global Partners</a>
                </li>
                <li>
                    <a href="press.php">News</a>
                </li>

                 <?php /* ?>
                <li>
                    <a <?php echo (IS_PROD) ? "onclick='$(\"#demo_site_msg\").modal(\"show\")' href='javascript:void(0);'" : 'href="https://newcallcenter.takemethere.ca/login"'; ?> >Sign In</a>
                </li>
                                <?php */ ?>
                <?php /* ?>
                <li>
                    <a <?php echo (IS_PROD) ? "onclick='$(\"#demo_site_msg\").modal(\"show\")' href='javascript:void(0);'" : 'href="register.php"'; ?> >Register</a>
                </li>
                <?php */ ?>
                <li><a onclick="$('#modal__contact').modal('show')" href="javascript:void(0);" class="reg-btn">Contact Us</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>
<!-- Navigation End -->
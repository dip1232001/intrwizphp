<?php include_once 'partials/head.php'; ?>
<?php include_once 'partials/navbar.php'; ?>

<!-- Body Section Start -->
<div id="key-feature-body-wrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12">
                <h2>Value Proposition</h2>
                <h3>Why use Intrwiz?</h3>
                <p class="curve-txt">Turn-Key Technology allowing your agents to compete with OTA's</p>

                <ul>
                    <li>State of the art turn-key technology that allows you to have a white label website enabling you to fully function like an OTA with  your own user hierarchies, booking engines, price controls and GDS.</li>
                    <li>Create and manage multiple client inquiries on a productive, organised dashboard with access to easy reports, intelligence for upselling campaigns.</li>
                    <li>Queue your booking engines, GDS feeds automatically generate dynamic,interactive itineraries / trip planners for existing bookings on a single dashboard.</li>
                    <li>Admin module /Control Panel access for full range of customization including platform pricing, look and feel, user management, supplier management booking enablement, custom messaging etc.</li>
                    <li>A one stop shopping experience for your clients to engage in real time with Friends and Family</li>
                    <li>A one stop product researching experience for agents to search and save travel products</li>
                    <li>Interactive itineraries are updated real-time to show booking and search efforts</li>
                </ul>

                <h2>Enterprise Level Tools to manage leads through all stages</h2>

                <ul>
                    <li>Keep better organised with our agent  trip dashboard for leads, sales and  productive results</li>
                    <li>KPI’s on each lead are tracked to provide travel agents with business intelligence on each of their customers to help convert.</li>
                    <li>Targeted marketing and messaging in tandem with your handpicked travel supplier content based on clients behavioral, social and transactional data aimed at conversion and up-sell</li>
                    <li>Create, manage group or individual bookings, inquiries and bulk user based campaigns for clients. Promote specials and track all of the above on one dashboard</li>
                    <li>Manager access to back-end provides Key Performance Indicators on all agent leads, numbers and activity</li>
                    <li>Agent access to back-end activity tracking delivers real-time data on leads per day, conversion, incremental customers and much more</li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12">
                <div class="rgt-panel">
                    <div class="link-section">
                        <ul>
                            <li>
                                <h2>PDF Presentation Deck</h2>
                                <p><span>Intrwiz by ArrivalGuides</span> - 1.28 MB</p>
                            </li>
                            <li>
                                <h2>Five Questions with Intrwize</h2>
                                <p><span>Five Questions with Intrwize</span> - 565.29 KB</p>
                            </li>
                        </ul>
                    </div>
                    <div class="agent-features-section">
                        <h2>Travel Agent Features</h2>
                        <p>Value Proposition</p>
                        <p>Enterprise Level Tools to manage leads through all stages</p>
                        <p>Extended your reach and drive incremental sales</p>
                    </div>
                    <div class="support-section">
                        <img src="img/icon-support.png" class="img-responsive">
                        <p>Questions?</p>
                        <p><a href="#">support@intrwiz.com</a></p>
                    </div>
                </div>
            </div>
        </div>            
    </div>
</div>
<!-- Body Section End -->

<?php include_once 'partials/footer.php'; ?>

</body>
</html>

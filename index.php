<?php include_once 'partials/head.php'; ?>
<?php include_once 'partials/navbar.php'; ?>
<!-- Header Carousel Start -->
<header id="myCarousel1" class="carousel slide">
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <div class="item active">
            <div class="fill" style="background-image:url('img/home/banner-homepage.jpg');"></div>
            <div class="carousel-caption">
                <h2>Your Travel Business is <span>Ready For Take-off</span></h2>
                <p>Introducing the most sophisticated travel platform in the world with full content and 24/7 support designed by industry experts specially to give travel agents a decisive advantage in the marketplace</p>
            </div>
        </div>
    </div>
</header>
<!-- Header Carousel End -->

<!-- Core Pieces Section Start -->
<div class="section-core-pieces">
    <div class="container">
        <h2 class="text-center">The 3 core pieces of your new system:</h2>
        <ul>
            <li><img src="img/home/icon-paper-plane.png"> Online Booking Platform</li>
            <li><img src="img/home/icon-dashboard.png"> Dashboard</li>
            <li><img src="img/home/icon-controlpanel.png"> Control Panel</li>
        </ul>
    </div>
</div>
<!-- Core Pieces Section End -->       

<!-- Client Portal Section Start -->
<div class="section-client-portal">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 no-pad">
            <img src="img/home/laptop-clientfacingportal.png" class="img-responsive">
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="gray-box-contain">
                <h3><img src="img/home/icon-paper-plane.png">Your Online Booking Platform</h3>
               <p>State of the Art Collaborative Interactive Booking Solutions for Air, Hotel, Car, Activities, Tours and Cruise directly contented to hundreds of suppliers in the Intrwiz Eco System. </p>

                <p>Each search automatically creates a Branded Interactive Micro Site directly connected to your Agent Dashboard allowing you to seamlessly engage in real time with each customer through the planning and booking process.</p>

                <a href="agent-platform.php#client-facing-portal" class="btn-details btn-details-sm">See Details</a>
            </div>
        </div>
    </div>
</div>
<!-- Client Portal Section End -->

<!-- Dashboard Section Start -->
<div class="section-dashboard">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 no-pad hidden-md hidden-lg"><img src="img/home/laptop-dashboard.png" class="img-responsive"></div>
        <div class="col-lg-6 col-md-6 col-sm-12 text-right">
            <div class="wh-box-contain">
                <h3><img src="img/home/icon-dashboard.png">Your Interactive Agent Dashboard</h3>
               <p>Engage and manage your customers seamlessly on your Intrwiz Interactive Agent Dashboard where you can interact with each of your customers in real time providing a unique end to end experience that will keep your customers coming back to you forever.</p>

<p>Each Branded Interactive Customer Micro Site is connected to your agent dashboard providing you with unparalleled business intelligence to convert all your opportunities and maximize sales on each trip.  </p>

<p>Branded Micro Sites can be created via each booking on your GDS, every search on the Intrwiz Online Booking Solutions or can be created from the Agent Dashboard. </p>

                <a href="agent-platform.php#agent-dashboard" class="btn-details btn-details-sm pull-right agentDashboardLink">See Details</a>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 no-pad hidden-sm hidden-xs"><img src="img/home/laptop-dashboard.png" class="img-responsive"></div>
    </div>
</div>
<!-- Dashboard Section End -->

<!-- Control Panel Section Start -->
<div class="section-control-panel">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 no-pad">
            <img src="img/home/laptop-controlpanel.jpg" class="img-responsive">
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="gray-box-contain">
                <h3><img src="img/home/icon-controlpanel.png">Your Control Panel  </h3>
                <p>Customize your client facing portal and each Branded Interactive Customer Micro Site including look and feel, messaging, suppliers, booking engines, pricing, email notifications and many other intuitive features to help you create a unique branded experience for your customers.</p>
                <a href="agent-platform.php#control-panel" class="btn-details btn-details-sm">See Details</a>
            </div>
        </div>
    </div>
</div>
<!-- Control Panel Section End -->

 <!-- Commisions Section Start -->
    <?php include_once 'partials/intrwizEcosystem.php'; ?>
    <!-- Commisions Section End -->

<!-- Additional Products Section Start -->
<div class="section-additional-product">
    <div class="container">
        <h2 class="text-center">Additional Products</h2>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="gray-box">
                    <h3>Closed User Group Affinity Site</h3>
                    <ul>
                        <li>Branded Intrwiz Site at net rates.</li>
                        <li>Control panel to fully Customize platform including setting markups to the net rates.</li>
                        <li>Dashboard to manage bookings.</li>
                        <li>24/7 call center support (optional)</li>
                        <li>Built in collaborative itinerary powered by ArrivalGuides.</li>
                    </ul>
                    <div class="btn-row text-center">
                        <a href="affinity-site.php" class="btn-details">See Details</a>
                    </div>
       
                    <div class="our-client-row">
                        <p class="green-txt text-center"><a href="#">See Our Sample Clients</a></p>
                        <ul class="client-logo-row">
                            <li><a href="<?php echo ATCOST_LINK?>" target="_blank"><img src="img/home/logo-atcosttravel.png"></a></li>
                            <li><a href="<?php echo JUBILEE_LINK?>" target="_blank"><img src="img/JHA-logo-color.svg"></a></li>
                            <li><a href="<?php echo AIRMILES_LINK?>" target="_blank"><img src="img/home/logo-airmiles.png"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="gray-box">
                    <h3>Full end to end Custom Online Booking platforms</h3>
                    <ul>
                        <li>Customized Customer Facing Solution</li>
                        <li>Control panel to set up or adjust markups.</li>
                        <li>Dashboard to manage bookings.</li>
                        <li>24/7 call center support (optional)</li>
                        <li>Built in collaborative itinerary powered by ArrivalGuides.
                    </ul>
                    <div class="btn-row text-center">
                        <a href="full-end-to-end-custom.php" class="btn-details">See Details</a>
                    </div>
                    <div class="our-client-row">
                        <p class="green-txt text-center"><a href="#">See Our Sample Clients</a></p>
                        <ul class="client-logo-row">
                            <li><a href="<?php echo AG_LINK?>" target="_blank"><img src="img/home/logo-arrivalguides.png"></a></li>
                            <li><a href="<?php echo VIO_LINK?>" target="_blank"><img src="img/home/logo-vio.png"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="gray-box">
                    <h3>Collaborative Itinerary Solution</h3>
                    <ul>
                        <li>Maximize Ancillary Revenue Streams.</li>
                        <li>Provide a world class experience to your customers.</li>
                        <li>Integrated with Intrwiz Call Centre Dashboard to seamlessly manage each booking.</li>
                        <li>24/7 call center support (optional)</li>
                        <li>Loaded with Rich destination content powered by ArrivalGuides.</li>
                    </ul>
                    <div class="btn-row text-center">
                        <a href="collaborative-sollution.php" class="btn-details">See Details</a>
                    </div>
                    <div class="our-client-row hide">
                        <p class="pink-txt text-center"><a href="#">View Live Site</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Additional Products Section End -->

<!-- Testimonial Section Start -->
<?php if (!IS_PROD) { ?>
    <div class="section-testimonial hide">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <h2 class="text-center">What Travel Agents are Saying About<br>Intrwiz</h2>
                    <div class="slider-wraper">                        
                        <div id="myCarousel" class="carousel slide">                             
                            <ol class="carousel-indicators">
                                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                <li data-target="#myCarousel" data-slide-to="1"></li>
                                <li data-target="#myCarousel" data-slide-to="2"></li>
                            </ol>

                            <!-- Carousel items -->
                            <div class="carousel-inner">                                    
                                <div class="item active">
                                    <div class="row-fluid">
                                        <div class="span3">
                                            <h3>Debbie</h3>
                                            <p>Travel Agent</p>
                                            <div class="comment-wrap">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores commodi.</p>
                                            </div>
                                        </div>
                                        <div class="span3">
                                            <h3>Frank</h3>
                                            <p>Travel Agent</p>
                                            <div class="comment-wrap">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores commodi.</p>
                                            </div>
                                        </div>
                                        <div class="span3">
                                            <h3>Danny</h3>
                                            <p>Travel Agent</p>
                                            <div class="comment-wrap">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores commodi.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--/item-->

                                <div class="item">
                                    <div class="row-fluid">
                                        <div class="span3">
                                            <h3>Debbie</h3>
                                            <p>Travel Agent</p>
                                            <div class="comment-wrap">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores commodi.</p>
                                            </div>
                                        </div>
                                        <div class="span3">
                                            <h3>Frank</h3>
                                            <p>Travel Agent</p>
                                            <div class="comment-wrap">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores commodi.</p>
                                            </div>
                                        </div>
                                        <div class="span3">
                                            <h3>Danny</h3>
                                            <p>Travel Agent</p>
                                            <div class="comment-wrap">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores commodi.</p>
                                            </div>
                                        </div>
                                    </div><!--/row-fluid-->
                                </div><!--/item-->

                                <div class="item">
                                    <div class="row-fluid">
                                        <div class="span3">
                                            <h3>Debbie</h3>
                                            <p>Travel Agent</p>
                                            <div class="comment-wrap">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores commodi.</p>
                                            </div>
                                        </div>
                                        <div class="span3">
                                            <h3>Frank</h3>
                                            <p>Travel Agent</p>
                                            <div class="comment-wrap">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores commodi.</p>
                                            </div>
                                        </div>
                                        <div class="span3">
                                            <h3>Danny</h3>
                                            <p>Travel Agent</p>
                                            <div class="comment-wrap">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores commodi.</p>
                                            </div>
                                        </div>
                                    </div><!--/row-fluid-->
                                </div><!--/item-->
                            </div>                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<!-- Testimonial Section End -->

<!-- Caption Section Start -->
<div class="section-caption">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 text-center">Smart, Simple, Social</div>
        </div>
    </div>
</div>
<!-- Caption Section END -->

<?php include_once 'partials/footer.php'; ?>

</body>
</html>
